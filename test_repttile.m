%% test_repptile.m - a sample code for repttile detection and BoVW encoding
%
% A. Torii, torii@ctrl.titech.ac.jp
% 15/06/2013
%
close all; clear all;

dataset = 'pitts';
%  dataset = 'inria';

%--- paths to the libraries
addpath ./yael_v300/matlab
addpath ./practical-instance-search
if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end
%%
% 
% # ITEM1
% 
% # ITEM1
% # ITEM2
% 
% # ITEM2
% 

%--- parameters 
ps.drtype = 'RSIFT';  % type of feature (default)
ps.vvsize = 100000;   % size of visual vocabulary
ps.topN     = 50;     % max. kNN assignment ("K" in the paper)
ps.edgdthr  = 10;     % threshold of max. feature distances ("c")
ps.scalethr = 0.5;    % threshold of scale differences ("sigma")
ps.wmax = 3;          % max. number of adaptive assignments ("k_max")
ps.cutoff_thr = 1;    % threshold of truncating bovw weights ("T")

data_path = './data/';
idf_fn = [];
switch dataset
  case 'pitts'
    imfn1 = [data_path '000000_pitch1_yaw2.jpg'];
    idf_fn = [data_path 'pitts_100K_AA_idf.mat']; 
    vv_fn = [data_path 'vv_' ps.drtype '_K' num2str(ps.vvsize) '_S2_pitts.mat'];
    vlft = at_retrieve_vlparams(ps.drtype);
  case 'sf'
    imfn1 = [data_path '0458.jpg'];
    idf_fn = [data_path 'sf_100K_AA_idf.mat']; 
    vv_fn = [data_path 'vv_' ps.drtype '_K' num2str(ps.vvsize) '_S2_sf.mat'];
    vlft = at_retrieve_vlparams(ps.drtype,'OR',false);
  case 'inria'
    imfn1 = [data_path '149902.jpg'];
    vv_fn = [data_path 'vv_' ps.drtype '_K' num2str(ps.vvsize) '_S2_sf.mat'];
    vlft = at_retrieve_vlparams(ps.drtype);    
end
tic;
%--- load visual vocabulary
load(vv_fn);
CX = yael_fvecs_normalize(single(CX));
toc
tic;
%--- compute repttile
[CC, f, assgn_kNN, img] = at_img2repttile(CX, imfn1, ps, vlft);
% input (vocab, image, params for soft assignment,vl_feat param for feature extraction)
% ouput (repttile and all feature in repttile, f keypoints, assign_kNN 50 closest visual vocab, image)
toc
tic;
%--- compute (weighted and normalized) bovw
[v, K] = at_makebovw(CC, assgn_kNN, idf_fn, ps);
%[v_n] = make_normal_bowv(assgn_kNN, idf_fn, ps);
% output (a colmun vector of the weighted BoVW., number of assignment for each feature)
toc
%--- visualization
plot(1:length(v), v);
% plot(1:length(v_n), v_n);
figure;
imshow(img,'border','tight'); hold on;
colormap('gray'); axis image; axis off; hold on;
plot(f(1,:),f(2,:),'o','MarkerSize',4,'MarkerFaceColor','w','MarkerEdgeColor','k'); %[.99 .99 .99]);
set(gcf,'position',[100 500 320 280]);
title('Detected features');

figure;
Nmax = min(10,length(CC));
ccid = CC(1:Nmax);
C = colormap('jet');
cw = ceil(linspace(56,9,Nmax));
imshow(img,'border','tight'); hold on;
colormap('gray'); axis image; axis off; hold on;
for ii=Nmax:-1:1
  pts = f(1:2,ccid{ii});
  plot(pts(1,:),pts(2,:),'o','MarkerSize',4,'MarkerFaceColor',C(cw(ii),:),'MarkerEdgeColor','k');
end;
set(gcf,'position',[440 500 320 280]);
title('Detected repttles (10 lagest)');

figure;
C = colormap('jet');
imshow(img,'border','tight'); hold on;
colormap('gray'); axis image; axis off; hold on;
clr = 'rgb';
for pp=1:3
  pts = f(1:2,K==pp);
  plot(pts(1,:),pts(2,:),'o','MarkerSize',4,'MarkerFaceColor',clr(pp),'MarkerEdgeColor','k');
end;
set(gcf,'position',[780 500 320 280]);
title('Features with one (red), two (green), or three (blue) assignments');

%% copy cong thuc lam slide trinh bay, tut cua code, 


