
if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end


homographyGraphPath = './data/bow_hessaff_k_max_3_with_burst/homographyGraph.mat';
inlierGraphPath = './data/bow_hessaff_k_max_3_with_burst/inlierGraph.mat';
relationGraphPath = './data/bow_hessaff_k_max_3_with_burst/relationGraph.mat';
datasetKeyPointPath = './data/kp/oxbuilding';

[~, datasetKeyPointFiles] = readImageDir(datasetKeyPointPath);

inlierGraph = load(inlierGraphPath);
inlierGraph = inlierGraph.inlierGraph;
relationGraph = load(relationGraphPath);
relationGraph = relationGraph.relationGraph;

nDatasetImage = size(inlierGraph, 1);
homographyGraph = cell(nDatasetImage, nDatasetImage);

if matlabpool('size') == 0
    matlabpool open 4
end


for i=1:nDatasetImage
    tic;
    queryKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(i).name]);
    connectedImages = find(relationGraph(i,:) > 0);
    for j=1:length(connectedImages)
        imgIdx = connectedImages(j);
        connectedImageMatches = inlierGraph{i, imgIdx};
        connectedImageKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(imgIdx).name]);
        [~, H_query_dataset] = geometricVerification(queryKP,connectedImageKP,connectedImageMatches,'numRefinementIterations', 3);
        homographyGraph{i,imgIdx} = H_query_dataset;
    end
    toc
end
save(homographyGraphPath, 'homographyGraph');


% createDir('./data/temp');
% cellfun(@(x) copyfile(['./data/paris_building/' x(1:end-4) '.jpg'],'./data/temp'), diff,  'UniformOutput', false);

