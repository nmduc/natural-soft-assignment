configFile = './config.json';
loadLibraries();
configString = readJSONFile(configFile);
configParams = JSON.parse(configString);

% pathPrefix = './data/bow_hessaff_k_max_3_perdoch';
pathPrefix = configParams.pathPrefix;
queryImageVWPath = [pathPrefix '/' configParams.queryImageVWPath];
datasetImageVWPath = [pathPrefix '/' configParams.datasetImageVWPath];

datasetImagePath = configParams.datasetImagePath;
% datasetFeaturePath = './data/quantize/oxbuilding-50';
% queryFeaturePath = './data/quantize/query-50';
datasetFeaturePath =  configParams.datasetFeaturePath;
queryFeaturePath = configParams.queryFeaturePath;

tfIdfPath = [pathPrefix '/' configParams.tfIdfPath];

datasetKeyPointPath = configParams.datasetKeyPointPath;
queryKeyPointPath = configParams.queryKeyPointPath;
 rankListPath = configParams.rankListPath;
 createDir(rankListPath);

idf = load(tfIdfPath);
idf = idf.tf_idf;

[nQueryImages, queryFiles] = readImageDir(queryImageVWPath);
[nDatasetImage, datasetFiles] = readImageDir(datasetImageVWPath);
[~, datasetImageFiles] = readImageDir(datasetImagePath);

[~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
[~, queryFeatureFiles] = readImageDir(queryFeaturePath);
[~, datasetKeyPointFiles] = readImageDir(datasetKeyPointPath);
[~, queryKeyPointFiles] = readImageDir(queryKeyPointPath);
%iImage = 10;

if matlabpool('size') == 0
    matlabpool open 2
end


datasetVects = sparse(1e6, nDatasetImage);
parfor i=1:nDatasetImage
    dataset_vector = load([datasetImageVWPath '/' datasetFiles(i).name]);
    datasetVects(:,i) = dataset_vector.v;
    i
end

datasetVects = spdiags(idf, 0, 1e6, 1e6)*datasetVects;
datasetVects = datasetVects;



%%
KNN_SpatialRE = 20;
for j=1:nQueryImages
    tic;
    query_vector = load([queryImageVWPath '/' queryFiles(j).name]);
    query_vector = spdiags(idf, 0, 1e6, 1e6)*query_vector.v;
    queryKP = loadKeyPoint([queryKeyPointPath '/' queryKeyPointFiles(j).name]);
    queryFeature = loadFeatures([queryFeaturePath '/' queryFeatureFiles(j).name]);
%     score_result = zeros(nDatasetImage, 1);
%     tempVal = repmat(query_vector, 1, nDatasetImage);
%     binDiff = (datasetVects - tempVal) < 0;
%     datasetVectsRes = datasetVects.*binDiff;

    score_result = query_vector'*datasetVects;
%     score_result = score_result.*hamming_dis;
    
    [~, matchIdx] = sort(score_result, 'descend'); %for vector metric
    nTop = 200;
    geometricRerankSubset = matchIdx(1:nTop);
    inliersCount = zeros(nTop,1);
    inlierBins = sparse(1e6, 1);
    for i=1:nTop
        datasetKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(geometricRerankSubset(i)).name]);
        datasetFeature = loadFeatures([datasetFeaturePath '/' datasetFeatureFiles(geometricRerankSubset(i)).name]);
%         datasetMatchesWords = matchWords(queryFeature, datasetFeature);
        datasetMatchesWords = matchWords_n_head(queryFeature, datasetFeature);
        datasetInliers = geometricVerification(queryKP,datasetKP,datasetMatchesWords,'numRefinementIterations', 3);
        inliersCount(i,1) = numel(datasetInliers);
%         inliersCount(i,1) = 0;
        if inliersCount(i,1) > 20
            verifiedBins = datasetFeature(1:3, datasetMatchesWords(2, datasetInliers));
            verifiedBins = verifiedBins(:);
            verifiedBins = sparse(verifiedBins, ones(numel(verifiedBins),1), ones(numel(verifiedBins),1), 1e6, 1);
            inlierBins = inlierBins + verifiedBins;
%             inliersCount(i,1) = sum(idf(datasetFeature(1,datasetMatchesWords(2, datasetInliers)),1));
        end
    
%         datasetNearestMatchesWords = nearestmatchedword(datasetMatchesWords,queryKP,datasetKP);
%         if( not(cellfun('isempty',datasetNearestMatchesWords)))
%             vQuery = ConvertBinsToVectors(datasetNearestMatchesWords{1});
%             vData = ConvertBinsToVectors(datasetNearestMatchesWords{2});
%             inliersCount(i,1) = distEmd(vQuery',vData');
%         end;
    end
%     inlierBinsPos = find(inlierBins>0);
%     query_vector(inlierBinsPos) = query_vector(inlierBinsPos).*(inlierBins(inlierBinsPos));
%     score_result = query_vector'*datasetVects;
%     [~, matchIdx] = sort(score_result, 'descend');

    [~, inlierOrder] = sort(inliersCount, 'descend');
    matchIdx(1:nTop) = matchIdx(inlierOrder);

    %     matchIdx = query_geo_verifi_inlier_vect(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},datasetVects, queryFeature,queryKP);
    
    fileId = fopen([rankListPath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
    for i=1:nDatasetImage
        filename = datasetImageFiles(matchIdx(i)).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
    end
    fclose(fileId);
    toc
end
calmapBatch(rankListPath);