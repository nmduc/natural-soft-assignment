function [ BoWModel ] = loadHDF5BoWModel( modelPath )
%LOADHDF5BOWMODEL Summary of this function goes here
%   Detailed explanation goes here
    modelInfo = h5info(modelPath);
    BoWModel = h5read(modelPath, [ '/' modelInfo.Datasets.Name]);
    BoWModel = yael_fvecs_normalize(single(BoWModel));
end

