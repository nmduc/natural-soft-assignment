configFile = './config.json';
loadLibraries();
configString = readJSONFile(configFile);
configParams = JSON.parse(configString);

pathPrefix = configParams.pathPrefix;
% queryImageVWPath = './data/bow_hessaff_k_max_3_with_burst/query';
% pathPrefix = './data/HCM';
queryImageVWPath = [pathPrefix '/' configParams.queryImageVWPath];
% dataset_imageVW_path = './data/visualwordfeatures';
datasetImageVWPath = [pathPrefix '/' configParams.datasetImageVWPath];
datasetImagePath = configParams.datasetImagePath;
% datasetImagePath  = './data/HCM/HCMlandmark';
tfIdfPath = [pathPrefix '/' configParams.tfIdfPath];

rankListPath = configParams.rankListPath;
createDir(rankListPath);

alpha = str2double(configParams.alpha);

tf_idf = load(tfIdfPath);
tf_idf = tf_idf.tf_idf;

[nQueryImages, queryFiles] = readImageDir(queryImageVWPath);
[nDatasetImage, datasetFiles] = readImageDir(datasetImageVWPath);
[~, datasetImageFiles] = readImageDir(datasetImagePath);
%iImage = 10;

datasetVects = sparse(1e6, nDatasetImage);
parfor i=1:nDatasetImage
    dataset_vector = load([datasetImageVWPath '/' datasetFiles(i).name]);
    datasetVects(:,i) = dataset_vector.v;
    i
end

datasetVects = spdiags(tf_idf, 0, 1e6, 1e6)*datasetVects;

parfor j=1:nQueryImages
    tic;
    query_vector = load([queryImageVWPath '/' queryFiles(j).name]);
    query_vector = spdiags(tf_idf, 0, 1e6, 1e6)*query_vector.v;
    
    minVects = min(datasetVects, repmat(query_vector, 1, nDatasetImage));
    
    weight = sum(reshape(datasetVects, [], 1)) / sum(reshape(minVects, [], 1)) * alpha;
    
    score_result = sum(datasetVects) - weight * sum(minVects);
    
    [~, matchIdx] = sort(score_result);
    fileId = fopen([rankListPath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
    for i=1:nDatasetImage
        filename = datasetImageFiles(matchIdx(i)).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
    end
    fclose(fileId);
    toc
end
calmapBatch(rankListPath);