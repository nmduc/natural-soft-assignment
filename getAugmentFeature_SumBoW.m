% starter

if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end
% 
imagePath = './data/oxbuild_images';
datasetPrefix = './data/bow_hessaff_k_max_3_perdoch';
datasetVectsPath = [datasetPrefix '/oxbuilding'];
augmentVectorResultPath = [datasetPrefix '/' 'augmentResultOxbuildingSum'];
createDir(augmentVectorResultPath);
% 
datasetImagePath = './data/oxbuild_images';
% datasetFeaturePath = './data/oxbuilding-50';
% datasetKPPath = './data/kp/oxbuilding';
datasetFeaturePath = './data/quantize/oxbuild-perdoch-50';
datasetKPPath = './data/oxbuild_perdoch/kp';
inlierGraphPath = [datasetPrefix '/inlierGraph.mat'];
relationGraphPath = [datasetPrefix '/relationGraph.mat'];
homographyGraphPath = [datasetPrefix '/homographyGraph.mat'];


[nDatasetImage, datasetFiles] = readImageDir(datasetVectsPath);
[~, imageFiles] = readImageDir(imagePath);
[~, datasetImageFiles] = readImageDir(datasetImagePath);
% 
[~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
[~, datasetKPFiles] = readImageDir(datasetKPPath);


inlierGraph = load(inlierGraphPath);
inlierGraph = inlierGraph.inlierGraph;
relationGraph = load(relationGraphPath);
relationGraph = relationGraph.relationGraph;
homographyGraph = load(homographyGraphPath);
homographyGraph = homographyGraph.homographyGraph; % homography map point from query image -> datasetImage (i->j)

imageFeatures = cell(nDatasetImage, 1);
for i=1:nDatasetImage
    temp = load([datasetFeaturePath '/' datasetFeatureFiles(i).name]);
    imageFeatures{i,1} = temp.bins(1:3, :);
    i
end

imgSizes = zeros(2,nDatasetImage);
tic;
for i=1:nDatasetImage
    temp = size(imread([imagePath '/' imageFiles(i).name]));
    imgSizes(:,i) = temp(1:2);
end
toc

weights = 1./2.^((1:3) - 1);
range = [1:3];
% range = 1:3;
% datasetVects = sparse(1e6, nDatasetImage);
% parfor i=1:nDatasetImage
%     dataset_vector = load([datasetVectsPath '/' datasetFiles(i).name]);
%     datasetVects(:,i) = dataset_vector.v;
%     i
% end
load('./data/bow_hessaff_k_max_3_with_burst/datasetVectsRaw.mat')
datasetVects = datasetVectsRaw;
augmentedResultMatrix = sparse(1e6,nDatasetImage);
for i=1:nDatasetImage
    tic;
    directConnectVert = find(relationGraph(i, :) > 0);
    augmentResultVect = sum(datasetVects(:, directConnectVert), 2);
    
    
    fprintf('done calculating %d\n', i);
    augmentedResultMatrix(:, i) = augmentResultVect;
    save([augmentVectorResultPath '/' datasetFiles(i).name], 'augmentResultVect');
%     fprintf('done saving');
    toc
end
save('./data/bow_hessaff_k_max_3_with_burst/augmentMat.mat','augmentedResultMatrix','-v7.3');

