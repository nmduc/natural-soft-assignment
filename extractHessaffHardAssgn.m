% assgnKNNPath = './data/quantize/oxbuild-perdoch-50';
% bowResultPath = './data/bow_perdoch_hard_assign/oxbuilding';
assgnKNNPath = './data/quantize/oxbuild-query-perdoch-50';
bowResultPath = './data/bow_perdoch_hard_assign/query';

nVoc = 1e6;


createDir(bowResultPath);
[nImage, assgnKNNFiles] = readImageDir(assgnKNNPath);

for i = 1: nImage
    assgnKNN = load([assgnKNNPath '/' assgnKNNFiles(i).name]);
    assgnKNN = assgnKNN.bins(1,:);
    nFeature = size(assgnKNN, 2)
    v = sparse(assgnKNN, ones(nFeature,1), ones(nFeature,1), nVoc, 1);
    save([bowResultPath '/' assgnKNNFiles(i).name], 'v');
end