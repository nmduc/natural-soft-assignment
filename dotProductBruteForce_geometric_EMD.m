% if ~exist('vl_version','file'),
%   run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
% end
% 
% queryImageVWPath = './data/bow_hessaff_k_max_3_with_burst/query';
% dataset_imageVW_path = './data/bow_hessaff_k_max_3_with_burst/oxbuilding';
% 
% datasetImagePath = './data/oxbuild_images';
% datasetFeaturePath = './data/oxbuilding-50';
% queryFeaturePath = './data/query-50';
% idf_path = './data/bow_hessaff_k_max_3_with_burst/tf_idf.mat';
% tf_path = './data/bow_hessaff_k_max_3_with_burst/tf.mat';
% tf_query_path = './data/bow_hessaff_k_max_3_with_burst/tf_query.mat';
% 
% datasetKeyPointPath = './data/kp/oxbuilding';
% queryKeyPointPath = './data/kp/query';
rankListPath = './data/An_result_bow_hessaff_k_max_3';
createDir(rankListPath);
% 
% idf = load(idf_path);
% idf = idf.tf_idf;
% 
% [nQueryImages, queryFiles] = readImageDir(queryImageVWPath);
% [nDatasetImage, datasetFiles] = readImageDir(dataset_imageVW_path);
% [~, datasetImageFiles] = readImageDir(datasetImagePath);
% 
% [~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
% [~, queryFeatureFiles] = readImageDir(queryFeaturePath);
% [~, datasetKeyPointFiles] = readImageDir(datasetKeyPointPath);
% [~, queryKeyPointFiles] = readImageDir(queryKeyPointPath);
% %iImage = 10;
% 
% if matlabpool('size') == 0
%     matlabpool open 2
% end
% 
% 
% datasetVects = sparse(1e6, nDatasetImage);
% parfor i=1:nDatasetImage
%     dataset_vector = load([dataset_imageVW_path '/' datasetFiles(i).name]);
%     datasetVects(:,i) = dataset_vector.v;
%     i
% end
% 
% datasetVects = spdiags(idf, 0, 1e6, 1e6)*datasetVects;
% datasetVects = datasetVects;

KNN_SpatialRE = 20;
for j=1:nQueryImages
    tic;
    query_vector = load([queryImageVWPath '/' queryFiles(j).name]);
    query_vector = spdiags(idf, 0, 1e6, 1e6)*query_vector.v;
    queryKP = loadKeyPoint([queryKeyPointPath '/' queryKeyPointFiles(j).name]);
    queryFeature = loadFeatures([queryFeaturePath '/' queryFeatureFiles(j).name]);
    
    %Cosine similarity
    [score matchIdx] = query_cosin_similarity(datasetVects,query_vector);

    %---------
    %query verification
    %---------
    %Geo verification
    nTop = 100;
    matchIdx = query_geo_verifi(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},queryFeature,queryKP);
    
    nExpVect = 10;
    %---------
    %query expansion
    %---------
    %SVM
    %matchIdx = query_discrimative(datasetVects,matchIdx,nExpVect);
    %AVG 
    %matchIdx = query_avg_exp(datasetVects,score,matchIdx,nExpVect);
    
    % store result in files
    fileId = fopen([rankListPath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
    for i=1:nDatasetImage
        filename = datasetImageFiles(matchIdx(i)).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
    end
    fclose(fileId);
    toc
end