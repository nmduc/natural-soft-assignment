function [v, K] = at_makebovw(CC,assgn_kNN,idf_fn,opt)
% Compute BoVW by adaptive soft-assignment using repttiles
% 
% Input:
%   CC = a cell storing the indices to the features of each repttile.
%   assgn_kNN = a matrix storing indices of the topN nearest visual words 
%   per column.
%   idf_fn = a file name of IDF weights (ignored if not available)
%
% Output:
%   v = a colmun vector of the weighted BoVW.
%   K = labels for each feature. Features belonging to larger repttiles have
%   smaller values.
%
% A. Torii, torii@ctrl.titech.ac.jp
% 15/06/2013
% CC is the disjoint sets of repttile, store all of the disjoint sets, get
% length of each set resulting in m_f
assgn = assgn_kNN(1:opt.topN,:);
Np = size(assgn,2);
K = zeros(1,Np);
for jj=1:length(CC)
  id = CC{jj};
  K(id) = length(id);
end;
K = log((Np+1)./K);
K = ceil((K./max(K))*opt.wmax);

wbase = 1./(2.^(0:(opt.wmax-1))); % wmax = 3: maximum assignment is 3 so w_id components can only take 1, 1/2, 1/4
weights = cell(1,Np);
words = cell(1,Np);
for jj=1:Np
  idx = 1:K(jj);
  weights{jj} = wbase(idx);
  words{jj}   = assgn(idx,jj)';
end; % extract the k_f nearest neighbor from K=50 nearest neighbor
words = [words{:}];
% sum over all features of an image
v = sparse(double(words(:)), ones(1,length(words)), [weights{:}],opt.vvsize,1);
% sparse adds together elements in v that have duplicate subscripts in i and j.

%--- thresholding bursted weights
v = min(v,opt.cutoff_thr);
% ommit weights bigger than threshold

%--- IDF weighting 
if ~~exist(idf_fn,'file')
  r = load(idf_fn); DF = r.tf_idf; clear r;
%   DF(DF<=0) = 1; DF(DF>N) = N;
%   idf = log(N./double(DF));
  v = sparse(1:length(DF),1:length(DF),DF) * v;
end
v = at_svnorm(v);
