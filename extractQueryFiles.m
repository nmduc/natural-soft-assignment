%% load libraries
%--- paths to the libraries
addpath ./yael_v300/matlab
addpath ./practical-instance-search
if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end

%% define env var

%--- parameters 
ps.drtype = 'RSIFT';  % type of feature (default)
ps.vvsize = 1000000;   % size of visual vocabulary
ps.topN     = 50;     % max. kNN assignment ("K" in the paper)
ps.edgdthr  = 10;     % threshold of max. feature distances ("c")
ps.scalethr = 0.5;    % threshold of scale differences ("sigma")
ps.wmax = 3;          % max. number of adaptive assignments ("k_max")
ps.cutoff_thr = 1;    % threshold of truncating bovw weights ("T")
vlft = at_retrieve_vlparams(ps.drtype);

data_path = './data/queryfeaturehessaff';
model_path = './data/oxbuilding_codebook_l2_1000000_24464227_128_50it.hdf5'; % hdf5 file format
visual_word_encoding_feature = './data/queryvwhessaff';
image_feature_visual_word_assignment_path = './data/queryfeaturevwhessaff';
assgn_kNN_result = './data/assgnkNNqueryhessaff';
connectedcompresult = './data/connectedcomp/query';
createDir(connectedcompresult);
createDir(assgn_kNN_result);
createDir(image_feature_visual_word_assignment_path);
createDir(visual_word_encoding_feature);
%% load model from predefined files
BoWModel = loadHDF5BoWModel(model_path);

%% build feature graph and find repttile
% [nImages, fileNames, filePaths, nClass, classDir, imageDirVar] = countImagesInDir(data_path);
[nImages, imagePaths] = readImageDir(data_path);
% for i=1:nClass
%     createDir([visual_word_encoding_feature '/' classDir(i)]);
% end
kdTreePath = './data/kdtree.mat';
CXtree = [];
if exist(kdTreePath)==0
    CXtree = vl_kdtreebuild(BoWModel);
    save(kdTreePath, 'CXtree');
else
    CXtree = load(kdTreePath);
    CXtree = CXtree.CXtree;
end


for i=1:nImages
      [CC, f, assgn_kNN, img] = at_img2repttile(BoWModel, CXtree,[ data_path '/' imagePaths(i).name], ps, vlft);
%       [v, K] = at_makebovw(CC, assgn_kNN, '', ps);
     %[f, assign_kNN,img ] = soft_assignment(BoWModel, CXtree, [ data_path '/' imagePaths(i).name], ps, vlft, 3);
     %v = make_normal_bowv(assgn_kNN, '', ps);
%     parForSave([assgn_kNN_result '/' imagePaths(i).name '.mat'], assgn_kNN);
     parForSave([connectedcompresult '/' imagePaths(i).name '_connected.mat'], CC);
%     image_visual_word = [visual_word_encoding_feature '/' imagePaths(i).name '.mat'];
%     save(image_visual_word, 'v');
%     save([image_feature_visual_word_assignment_path '/' imagePaths(i).name '_features.mat'], 'assgn_kNN');
end




%%


