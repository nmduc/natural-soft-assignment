dataPath = './data/HCM/HCMlandmark';
savePath = './data/HCM/groundtruth';
[nImages, imageFiles] = readImageDir(dataPath);
idxArray = 1:nImages;
allNameTrunc = cellfun(@(x) x(1:end-14), {imageFiles.name},'UniformOutput', false);
classes = unique(allNameTrunc);
classAssign = cellfun(@(x) find(ismember(classes,x)), allNameTrunc,'UniformOutput', false);
classBins = cell(size(classes));



for i=1:nImages
    curImg = imageFiles(i).name;
    imgClass = classAssign{i};
    classBins{imgClass} = [classBins{imgClass}, {curImg}];
end

for i=7:length(classes)
    chosenImg = rand(1,5)*length(classBins{i});
end


