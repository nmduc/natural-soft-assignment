configFile = './config.json';
loadLibraries();
configString = readJSONFile(configFile);
configParams = JSON.parse(configString);
% 
pathPrefix = configParams.pathPrefix;
queryImageVWPath = [pathPrefix '/' configParams.queryImageVWPath];
datasetImageVWPath = [pathPrefix '/' configParams.datasetImageVWPath];
% 
datasetImagePath = configParams.datasetImagePath;
% datasetFeaturePath = './data/oxbuilding-50';
% queryFeaturePath = './data/query-50';
datasetFeaturePath =  configParams.datasetFeaturePath;
queryFeaturePath = configParams.queryFeaturePath;
tfIdfPath = [pathPrefix '/' configParams.tfIdfPath];


% 
% datasetKeyPointPath = './data/kp/oxbuilding';
% queryKeyPointPath = './data/kp/query';
datasetKeyPointPath = configParams.datasetKeyPointPath;
queryKeyPointPath = configParams.queryKeyPointPath;
rankListPath = configParams.rankListPath;
rankListWithScorePath = './data/result_bow_hessaff_k_max_3_query_expand_score';
createDir(rankListPath);
createDir(rankListWithScorePath);
% 
idf = load(tfIdfPath);
idf = idf.tf_idf;
% 
[nQueryImages, queryFiles] = readImageDir(queryImageVWPath);
[nDatasetImage, datasetFiles] = readImageDir(datasetImageVWPath);
[~, datasetImageFiles] = readImageDir(datasetImagePath);
% 
[~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
[~, queryFeatureFiles] = readImageDir(queryFeaturePath);
[~, datasetKeyPointFiles] = readImageDir(datasetKeyPointPath);
[~, queryKeyPointFiles] = readImageDir(queryKeyPointPath);
% %iImage = 10;
% 
if matlabpool('size') == 0
    matlabpool open 2
end
% 
% 
datasetVectsRaw = sparse(1e6, nDatasetImage);
parfor i=1:nDatasetImage
    dataset_vector = load([datasetImageVWPath '/' datasetFiles(i).name]);
    datasetVectsRaw(:,i) = dataset_vector.v;
    i
end
% 
datasetVects = spdiags(idf, 0, 1e6, 1e6)*datasetVectsRaw;
datasetVects = datasetVects;

KNN_SpatialRE = 20;
for j=1:nQueryImages
    tic;
    query_vector = load([queryImageVWPath '/' queryFiles(j).name]);
    query_vector = spdiags(idf, 0, 1e6, 1e6)*query_vector.v;
    queryKP = loadKeyPoint([queryKeyPointPath '/' queryKeyPointFiles(j).name]);
    queryFeature = loadFeatures([queryFeaturePath '/' queryFeatureFiles(j).name]);

    %Cosine similarity
    [score matchIdx] = query_cosin_similarity(datasetVects,query_vector);

    %---------
    %query verification
    %---------
    %Geo verification
    nTop = 100;
    [matchIdx, exp_query_vect, nVerifiedImage] = query_geo_verifi_inlier_vect(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},datasetVectsRaw, queryFeature,queryKP);
%     [matchIdx, exp_query_vect, nVerifiedImage] = query_geo_verifi_inlier_project(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},datasetVectsRaw, queryFeature,queryKP,query_vector);
    nExpVect = 10;
    %---------
    %query expansion
    %---------
    %SVM
    %matchIdx = query_discrimative(datasetVects,matchIdx,nExpVect);
    %AVG 

     [matchIdx, score_res] = query_avg_exp_with_verified_vector(datasetVects, score, matchIdx, nExpVect, exp_query_vect, query_vector, nVerifiedImage);
%     nTop = 100;
%     [matchIdx, exp_query_vect, nVerifiedImage] = query_geo_verifi_inlier_rerank(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},datasetVectsRaw, queryFeature,queryKP,query_vector);
    % store result in files
    fileId = fopen([rankListPath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
%     scoreFileId = fopen([rankListWithScorePath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
    for i=1:nDatasetImage
        filename = datasetImageFiles(matchIdx(i)).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
%         fprintf(scoreFileId,'%s\t %f \n', filename, full(score_res(i)));
    end
    fclose(fileId);
%     fclose(scoreFileId);
    toc
end
calmapBatch(rankListPath)