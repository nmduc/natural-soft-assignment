function matchIdx = query_geo_verifi(matchIdx,nTop,data_Path,data_Files,queryFeature,queryKP)

% data_Path = {datasetKeyPointPath; datasetFeaturePath};
% data_Files = { datasetKeyPointFiles ; datasetFeatureFiles};

datasetKeyPointPath = data_Path{1,1};
datasetFeaturePath = data_Path{1,2};
datasetKeyPointFiles = data_Files{1,1};
datasetFeatureFiles = data_Files{1,2};

geometricRerankSubset = matchIdx(1:nTop);
inliersCount = zeros(nTop,1);
for i=1:nTop
    datasetKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(geometricRerankSubset(i)).name]);
    datasetFeature = loadFeatures([datasetFeaturePath '/' datasetFeatureFiles(geometricRerankSubset(i)).name]);
    datasetMatchesWords = matchWords_n_head(queryFeature(1:3,:), datasetFeature(1:3,:));
    datasetInliers = geometricVerification(queryKP,datasetKP,datasetMatchesWords,'numRefinementIterations', 3);
    inliersCount(i,1) = numel(datasetInliers);

    i
end
[~, inlierOrder] = sort(inliersCount, 'descend');
matchIdx(1:nTop) = matchIdx(inlierOrder);