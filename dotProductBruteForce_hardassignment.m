if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end
% 
queryImageVWPath = './data/bow_origin_rootsift/query';
dataset_imageVW_path = './data/bow_origin_rootsift/oxbuilding';
datasetVectsSavePath = './data/bow_hessaff_k_max_3_with_burst/datasetVects_Aug.mat';
idf_path = './data/bow_hessaff_k_max_3_with_burst/tf_idf.mat';

% 
datasetImagePath = './data/oxbuild_images';
datasetFeaturePath = './data/oxbuilding-50';
queryFeaturePath = './data/query-50';

% 
datasetKeyPointPath = './data/kp/oxbuilding';
queryKeyPointPath = './data/kp/query';
rankListPath = './data/result_rootsift_hardassignment';
createDir(rankListPath);
% 
idf = load(idf_path);
idf = idf.tf_idf;
% 
[nQueryImages, queryFiles] = readImageDir(queryImageVWPath);
[nDatasetImage, datasetFiles] = readImageDir(dataset_imageVW_path);
[~, datasetImageFiles] = readImageDir(datasetImagePath);
% 
[~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
[~, queryFeatureFiles] = readImageDir(queryFeaturePath);
[~, datasetKeyPointFiles] = readImageDir(datasetKeyPointPath);
[~, queryKeyPointFiles] = readImageDir(queryKeyPointPath);
% %iImage = 10;
% 
if matlabpool('size') == 0
    matlabpool open 4
end
% 
% 
tic;
datasetVectsRaw = sparse(1e6, nDatasetImage);
for i=1:nDatasetImage
    dataset_vector = load([dataset_imageVW_path '/' datasetFiles(i).name]);
    datasetVectsRaw(:,i) = dataset_vector.v;
    i
end
toc
save('./data/bow_hessaff_k_max_3_with_burst/datasetVectsRaw.mat', 'datasetVectsRaw');

datasetVects = spdiags(idf, 0, 1e6, 1e6)*(datasetVectsRaw);
save(datasetVectsSavePath,'datasetVects');

KNN_SpatialRE = 20;
for j=1:nQueryImages
    tic;
    query_vector = load([queryImageVWPath '/' queryFiles(j).name]);
    query_vector = spdiags(idf, 0, 1e6, 1e6)*query_vector.v;
    queryKP = loadKeyPoint([queryKeyPointPath '/' queryKeyPointFiles(j).name]);
    queryFeature = loadFeatures([queryFeaturePath '/' queryFeatureFiles(j).name]);
    
    %Cosine similarity
    [score matchIdx] = query_cosin_similarity(datasetVects,query_vector);

    % store result in files
    fileId = fopen([rankListPath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
    for i=1:nDatasetImage
        filename = datasetImageFiles(matchIdx(i)).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
    end
    fclose(fileId);
    toc
end