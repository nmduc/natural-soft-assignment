% D = rdir('D:\Documents\DPM\newest_src\oxbuild_images\**\*.jpg');
% fil={D.name};  
% for k=1:numel(fil)
%   file=fil{k};
%   tmp = file(end-3:end-3);
% %   if (strcmp(tmp, '.'))
% %       continue;
% %   end;
%   new_file=strrep(file, '.jpg','.png');
%   
%   exe = 'D:\Documents\DPM\newest_src\OpenCV.exe';
%   cmd = sprintf('%s %s %s', exe, file, new_file);
%   dos(cmd);
% end

% extract_features('D:\Documents\DPM\newest_src\oxbuild_images');
% sampling('D:\Documents\DPM\newest_src\oxbuild_images', 'D:\Documents\DPM\newest_src\oxbuild_images\all_features.hdf5');
run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
addpath('C:/Users/Administrator/Desktop/Nguyen Minh Duc/Workplace/flann_matlab');
% database.comp_sim= struct('query_obj','fg+bg_0.1','feat_detr','-hesaff', 'feat_desc', '-rootsift -noangle',...
%   'clustering','akmeans','K',1000000,'num_samps',100000000,'iter',50,...
%   'build_params',struct('algorithm', 'kdtree','trees', 8, 'checks', 800, 'cores', 10),...
%   'video_sampling',1,'frame_sampling',1,'knn',1,'delta_sqr',6250,'db_agg','avg_pooling',...
%   'vocab','full','trim','notrim','freq','clip','weight','idf','norm','nonorm',...
%   'query_knn',3,'query_delta_sqr',6250,'query_num',-1,'query_agg','avg_pooling','dist','l2_ivf');

database.comp_sim= struct('query_obj','fg+bg_0.1','feat_detr','-hesaff', 'feat_desc', '-rootsift -noangle',...
  'clustering','akmeans','K',1000000,'num_samps',100000000,'iter',50,...
  'build_params',struct('algorithm', 'kdtree','trees', 8, 'checks', 800, 'cores', 10),...
  'video_sampling',1,'frame_sampling',1,'knn',50,'delta_sqr',6250,'db_agg','avg_pooling',...
  'vocab','full','trim','notrim','freq','clip','weight','idf','norm','nonorm',...
  'query_knn',3,'query_delta_sqr',6250,'query_num',-1,'query_agg','avg_pooling','dist','l2_ivf');

quant_struct = struct('quantize','kdtree','build_params',database.comp_sim.build_params,'knn',database.comp_sim.knn,'delta_sqr',database.comp_sim.delta_sqr);

% Load codebook from file hdf5
disp('Loading codebook file ...');
% centers = hdf5read('/home/bao/oxbuilding_codebook_l2_1000000_24464227_128_50it.hdf5','/clusters');
centers = hdf5read('./data/oxbuild_perdoch/Clustering_l2_1000000_13516675_128_50it.hdf5','/clusters');
dataset = single(centers);
[feat_len,hist_len] = size(dataset);
fprintf('Deduced cluster center info %d %d...\n', feat_len, hist_len);
kdtree_filename = fullfile('./data/HCM','flann_kdtree_perdoch.bin');
kdsearch_filename = fullfile('./data/HCM','flann_kdtree_search.mat');

if exist(kdtree_filename,'file')
		fprintf('Loading kdtree ...');
		kdtree = flann_load_index(kdtree_filename,dataset);
		load(kdsearch_filename);
	else
		fprintf('Building kdtree ...');
		[kdtree,search_params,speedup] = flann_build_index(dataset,quant_struct.build_params); 
		fprintf('save kdtree ...');
		flann_save_index(kdtree,kdtree_filename);
		save(kdsearch_filename,'search_params');
end

% image_dir = '/home/bao/oxford_building';
% quan_dir = '/home/bao/quantize/oxbuilding-50';
% subbow_dir = '/home/bao/subbow/oxbuilding-50';
%  image_dir = './data/oxbuild_query_perdoch/desc';
% quan_dir = './data/quantize/oxbuild-query-perdoch-50';
% subbow_dir = '/home/bao/subbow/paris-query-50';
% image_dir = './data/HCM/feature/desc';
% quan_dir = './data/HCM/quantize/HCM';
image_dir = './data/HCM/feature-query/desc';
quan_dir = './data/HCM/quantize-query/HCM';
% image_dir = './data/SUN/feature/desc';
% quan_dir = './data/SUN/quantize/SUN';


createDir(quan_dir);
% createDir(subbow_dir);

D = dir(fullfile(image_dir, '*.mat'));
fil={D.name};  

% Quantize cac feature cua moi frame su dung given codebook voi cau truc du lieu kdtree
for k=1:numel(fil)
    tic;
    clip_feat_file=fullfile(image_dir, fil{k});
    % duong dan den file chua feature cua mot shot
    if ~exist(clip_feat_file,'file')
        disp([clip_feat_file ' does not exist!']);
        continue;
    end

    quant_file = fullfile(quan_dir, fil{k});
    %load feature cua tung shot
%     try
        load(clip_feat_file, 'clip_desc');
        %quantize feature
%         [bins,sqrdists] = flann_search(kdtree,single(clip_desc),quant_struct.knn, search_params);
        [bins,sqrdists] = flann_search(kdtree,single(clip_desc),quant_struct.knn, search_params);
        % Save quant file
        save(quant_file, 'bins','sqrdists');
%     catch err
%         fprintf('Loi o line 74 -> Image:%s - %s\n', clip_feat_file, err.identifier);
%     end
    clear bins sqrdists	frame_desc clip_desc
    toc
end

% D = dir(fullfile(quan_dir, '*.mat'));
% fil={D.name};  

% Build Bag of Word cho tung Image
% for k=1:numel(fil)
%     quant_file = fullfile(quan_dir, fil{k});
%     subbow_file = fullfile(subbow_dir, fil{k});
%     
%     try
%         load(quant_file);
%         num_frame = length(bins);
%         % frame_bow to store bow of each frame
%         frame_bow = zeros(hist_len,1);
%         if quant_struct.knn>1 && quant_struct.delta_sqr ~= -1
%             frame_freq = zeros(hist_len,1);
%         end
%         if isempty(bins)
%             continue;
%         end
%         bin = reshape(bins(1:quant_struct.knn,:),1,[]);
%         if quant_struct.knn>1 && quant_struct.delta_sqr ~= -1
%             sqrdist = sqrdists(1:quant_struct.knn,:);
%             weis = exp(-sqrdist./(2*quant_struct.delta_sqr));
%             weis = weis./repmat(sum(weis,1),size(weis,1),1);  % philbin, Lost in Quantization
%             weis = reshape(weis,1,[]);
%             frame_freq = vl_binsum(frame_freq,double(ones(size(bin))),double(bin));
%         else
%             weis = ones(size(bin));
%         end
%         frame_bow = vl_binsum(frame_bow,double(weis),double(bin));
%         frame_bow = sparse(frame_bow);
%         
%         % Save quant_file and subbow_file
%         if ~exist('frame_freq','var')
%             save(subbow_file, 'frame_bow');
%         else
%             frame_freq = sparse(frame_freq);
%             save(subbow_file, 'frame_bow','frame_freq');
%         end
%     catch err
%         fprintf('Loi o line 118 -> Image:%s - %s\n', fil{k}, err.identifier);
%     end
%     
%     clear bins sqrdists frame_bow frame_freq weis bin bins sqrdists
% end
%     
% 
