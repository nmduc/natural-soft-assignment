function [f, desc] = readFeaturePerdoch(featurePath, imgName)
kpPath = [featurePath '/' 'kp' '/' imgName];
descPath = [featurePath '/' 'desc' '/' imgName];
load(kpPath);
load(descPath);
f = clip_kp;
desc = clip_desc;