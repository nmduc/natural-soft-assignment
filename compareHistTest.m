dbImage = 'pitt_rivers_000032';
queryImage = 'all_souls_5';
query_vector_path = './data/queryvw';
dataset_imageVW_path = './data/visualwordfeatures';
[nQueryImages, queryFiles] = readImageDir(query_vector_path);
[nDatasetImage, datasetFiles] = readImageDir(dataset_imageVW_path);
dbImageHist = [];
queryImageHist = [];
for i=1:nQueryImages
    if (strfind([queryFiles(i).name],queryImage) >= 1)
        queryImageHist = load([queryImageVWPath '/' queryFiles(i).name]);
        queryImageHist = queryImageHist.v;
        break;
    end
end
for i=1:nDatasetImage
    if (strfind([datasetFiles(i).name], dbImage)>=1)
        dbImageHist = load([dataset_imageVW_path '/' datasetFiles(i).name]);
        dbImageHist = dbImageHist.v;
    end
end

