configFile = './config.json';
loadLibraries();
configString = readJSONFile(configFile);
configParams = JSON.parse(configString);

pathPrefix = configParams.pathPrefix;
queryImageVWPath = [pathPrefix '/' configParams.queryImageVWPath];
datasetImageVWPath = [pathPrefix '/' configParams.datasetImageVWPath];

datasetAugmentPath = [pathPrefix '/' configParams.datasetAugmentPath];
datasetVectsSavePath = [pathPrefix  '/' configParams.datasetVectsSavePath];
% 
datasetImagePath = configParams.datasetImagePath;
datasetFeaturePath =  configParams.datasetFeaturePath;
queryFeaturePath = configParams.queryFeaturePath;
tfIdfPath = [pathPrefix '/' configParams.tfIdfPath];

% 
% datasetKeyPointPath = './data/kp/oxbuilding';
% queryKeyPointPath = './data/kp/query';
datasetKeyPointPath = configParams.datasetKeyPointPath;
queryKeyPointPath = configParams.queryKeyPointPath;
rankListPath = configParams.rankListPath;
createDir(rankListPath);
% 
idf = load(tfIdfPath);
idf = idf.tf_idf;
% 
[nQueryImages, queryFiles] = readImageDir(queryImageVWPath);
[nDatasetImage, datasetFiles] = readImageDir(datasetImageVWPath);
[~, datasetImageFiles] = readImageDir(datasetImagePath);
% 
[~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
[~, queryFeatureFiles] = readImageDir(queryFeaturePath);
[~, datasetKeyPointFiles] = readImageDir(datasetKeyPointPath);
[~, queryKeyPointFiles] = readImageDir(queryKeyPointPath);
[~, datasetAugmentFiles] = readImageDir(datasetAugmentPath);
% %iImage = 10;
% 
if matlabpool('size') == 0
    matlabpool open 4
end
% 
% 
tic;
datasetVectsRaw = sparse(1e6, nDatasetImage);
parfor i=1:nDatasetImage
    dataset_vector = load([datasetImageVWPath '/' datasetFiles(i).name]);
    datasetVectsRaw(:,i) = dataset_vector.v;
    i
end
toc
% save('./data/bow_hessaff_k_max_3_with_burst/datasetVectsRaw.mat', 'datasetVectsRaw');

tic;
datasetAugmentVects = sparse(1e6, nDatasetImage);
parfor i=1:nDatasetImage
    dataset_vector = load([datasetAugmentPath '/' datasetAugmentFiles(i).name]);
    datasetAugmentVects(:,i) =  dataset_vector.augmentResultVect;
    i
end
toc
% 
% datasetVects = spdiags(idf, 0, 1e6, 1e6)*(datasetVectsRaw);
temp = (datasetAugmentVects * spdiags(1./sqrt(sum(datasetAugmentVects.^2,1))', 0, nDatasetImage, nDatasetImage));
datasetVects = spdiags(idf, 0, 1e6, 1e6)*(datasetVectsRaw + (0.2).*temp );


% save(datasetVectsSavePath,'datasetVects');
% datasetVectNorm = sqrt(sum(datasetVects.^2, 1));
% datasetVects = datasetVects*spdiags(datasetVectNorm', 0, nDatasetImage, nDatasetImage);
% datasetVects = datasetVects;

KNN_SpatialRE = 20;
for j=1:nQueryImages
    tic;
    query_vector = load([queryImageVWPath '/' queryFiles(j).name]);
    query_vector = spdiags(idf, 0, 1e6, 1e6)*query_vector.v;
    queryKP = loadKeyPoint([queryKeyPointPath '/' queryKeyPointFiles(j).name]);
    queryFeature = loadFeatures([queryFeaturePath '/' queryFeatureFiles(j).name]);
    
    %Cosine similarity
    [score matchIdx] = query_cosin_similarity(datasetVects,query_vector);

    %---------
    %query verification
    %---------
    %Geo verification
    nTop = 200;
    [matchIdx, exp_query_vect] = query_geo_verifi_inlier_vect(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},datasetVects, queryFeature,queryKP);
    
    nExpVect = 10;
    %---------
    %query expansion
    %---------
    %SVM
    %matchIdx = query_discrimative(datasetVects,matchIdx,nExpVect);
    %AVG 
    matchIdx = query_avg_exp_with_verified_vector(datasetVects, score, matchIdx, nExpVect, exp_query_vect, query_vector);
%     nTop = 100;
%      [matchIdx, exp_query_vect] = query_geo_verifi_inlier_vect(matchIdx,nTop,{datasetKeyPointPath datasetFeaturePath},{ datasetKeyPointFiles datasetFeatureFiles},datasetVects, queryFeature,queryKP);
    % store result in files
    fileId = fopen([rankListPath '/' queryFiles(j).name(1:end-4) '.txt'], 'w');
    for i=1:nDatasetImage
        filename = datasetImageFiles(matchIdx(i)).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
    end
    fclose(fileId);
    toc
end
calmapBatch(rankListPath);