function matchIdx = query_multi_exp(datasetVects,matchIdx_p,nExpVect)

%query expansion (AVG)
score_result = 0;
%% AVG vector
exp_vectors = datasetVects(:,matchIdx_p(1,1:nExpVect));

for i=1:nExpVect
    [score_c ~] = query_geo(datasetVects,exp_vectors(:,i));
    score_result = score_result + score_c;
end;

%% New rank lisk
[~, matchIdx] = sort(score_result, 'descend'); %for vector metric

