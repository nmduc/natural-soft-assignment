bow_path = './data/bow_hessaff_k_max_3_with_burst/query';
tf_idf_path = './data/bow_hessaff_k_max_3_with_burst/tf_query.mat';
knn_path = './data/query-50';

[nImages, bowFiles] = readImageDir(bow_path);
[~, KNNfiles] = readImageDir(knn_path);

% options
ps.drtype = 'RSIFT';  % type of feature (default)
ps.vvsize = 1000000;   % size of visual vocabulary
ps.topN     = 50;     % max. kNN assignment ("K" in the paper)
ps.edgdthr  = 10;     % threshold of max. feature distances ("c")
ps.scalethr = 0.5;    % threshold of scale differences ("sigma")
ps.wmax = 5;          % max. number of adaptive assignments ("k_max")
ps.cutoff_thr = 1;    % threshold of truncating bovw weights ("T")

tf = sparse(ps.vvsize, nImages);

for i=1:nImages
    k = load([bow_path '/' bowFiles(i).name]);
    k = k.K;
    
    v = load([knn_path '/' KNNfiles(i).name]);
    v = v.bins;
    temp = [];
    nFeature = size(v,2);
    for j=1:nFeature
       temp = [temp;v(1:k(j),j)];
    end;
    tf(temp,i) = tf(temp,i) + 1;
    tf(:,1) = tf(:,i)/size(temp,1);
    i
end

% calculate idf vector
if exist(tf_idf_path,'file')==0
    save(tf_idf_path, 'tf');
end
