function D = distEmd( X, Y )

%Xcdf = cumsum(X,2);
%Ycdf = cumsum(Y,2);

m = size(X,1);
dimen = size(X,2);
D = zeros(m,1);
Z = X - Y;
sumEMD = zeros(m,1);
EMD =zeros(m,1);
for j = 1:dimen
    EMD = EMD + Z(:,j);
    sumEMD = sumEMD + abs(EMD);
end;



D = sum(sumEMD,1)/m;