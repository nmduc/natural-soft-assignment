function [score_result matchIdx] = query_cosin_similarity(datasetVects,query_vector)
% cosin
score_result = query_vector'*datasetVects;
[~, matchIdx] = sort(score_result, 'descend'); %for vector metric

% nData = size(datasetVects, 2);
% score_result = zeros(nData,2);
% % cosin
% score_result(:,1) = query_vector'*datasetVects;
% % same bin
% score_result(:,2) = double(query_vector > 0)'*double(datasetVects > 0);
% [~, matchIdx] = sortrows(score_result, [-1 -2]); %for vector metric