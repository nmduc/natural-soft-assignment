function patches = nearestmatchedword(datasetMatchesWords,queryKP,datasetKP)

nMatch = size(datasetMatchesWords,2);
nFeature_query= size(queryKP,2);
nFeature_data = size(datasetKP,2);

%query 
queryKP_match = queryKP(:,datasetMatchesWords(1,:));

queryKP_match_X = repmat(queryKP_match(1,:),nFeature_query,1);
queryKP_match_X = queryKP_match_X - repmat(queryKP(1,:),nMatch,1)';

queryKP_match_Y = repmat(queryKP_match(2,:),nFeature_query,1);
queryKP_match_Y = queryKP_match_Y - repmat(queryKP(2,:),nMatch,1)';

queryKP_match_a = repmat(queryKP_match(3,:),nFeature_query,1);
queryKP_match_b = repmat(queryKP_match(4,:),nFeature_query,1);
queryKP_match_c = repmat(queryKP_match(5,:),nFeature_query,1);

queryKP_rep = 0.3*queryKP_match_a.*queryKP_match_X.*queryKP_match_X + 2*queryKP_match_b.*queryKP_match_Y.*queryKP_match_X + 0.3*queryKP_match_c.*queryKP_match_Y.*queryKP_match_Y;
 
%data
queryKP_match = datasetKP(:,datasetMatchesWords(2,:));

queryKP_match_X = repmat(queryKP_match(1,:),nFeature_data,1);
queryKP_match_X = queryKP_match_X - repmat(datasetKP(1,:),nMatch,1)';

queryKP_match_Y = repmat(queryKP_match(2,:),nFeature_data,1);
queryKP_match_Y = queryKP_match_Y - repmat(datasetKP(2,:),nMatch,1)';

queryKP_match_a = repmat(queryKP_match(3,:),nFeature_data,1);
queryKP_match_b = repmat(queryKP_match(4,:),nFeature_data,1);
queryKP_match_c = repmat(queryKP_match(5,:),nFeature_data,1);

datasetKP_rep = 0.3*queryKP_match_a.*queryKP_match_X.*queryKP_match_X + 2*queryKP_match_b.*queryKP_match_Y.*queryKP_match_X + 0.3*queryKP_match_c.*queryKP_match_Y.*queryKP_match_Y;
 

% return boolean matrix
temp = [];
temp1 = [];
for i=1:nMatch
    temp = [temp {find(queryKP_rep(:,i)>0 & queryKP_rep(:,i)<1)'}];
    temp1 = [temp1 {find(datasetKP_rep(:,i) > 0 & datasetKP_rep(:,i)<1)'}];
end;

patches = {temp ; temp1};

            
