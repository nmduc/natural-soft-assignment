function extract_features(frame_path)
    feature_config = '-hesaff -rootsift -noangle';
    feat_len = 128;
    feature_dir = strrep(feature_config, '-', '');
    feature_dir = strrep(feature_dir, ' ', '_');
    exe = 'D:\Documents\DPM\compute_descriptors.exe'; 
    run('D:\Documents\DPM\newest_src\vlfeat-0.9.19\toolbox\vl_setup.m');
    sift_filename = '\temp.mat';
    D = rdir(fullfile(frame_path, '**', '*.png'));
    fil={D.name};  
    for k=1:numel(fil)
        file=fil{k};
        cmd = sprintf('%s %s -i %s -o1 %s', exe, strrep(feature_config,'root',''), file, sift_filename);
        dos(cmd);
        if ~exist(sift_filename, 'file')
            clip_kp = mat(3, 0);
            clip_desc = mat(128, 0);
        else
            [clip_kp,clip_desc] = vl_ubcread(sift_filename, 'format', 'oxford');
        end
        if ~isempty(strfind(feature_config,'rootsift'))
                    sift = double(clip_desc);
                    clip_desc = single(sqrt(sift./repmat(sum(sift), feat_len, 1)));
        end
        clip_frame = rgb2gray(imread(file));
        save(strrep(file, 'png', 'mat'), 'clip_kp', 'clip_desc', 'clip_frame', '-v7.3');
        
        dos(['del \temp.mat']);
    end