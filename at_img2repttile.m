function [CC,f,assgn_kNN,img] = at_img2repttile(CX,CXtree,imfn,ps,vlft,assgn_kNN_path)
% Extract features, assgn to visual vocabulary, and detect repttile.
% 
% Input:
%   CX = a visual vocabulary. 128 x ps.vvsize. 
%   imfn = an image file name.
%   ps = a structure storing the parameters of repttile detection.
%   vlft = a structure storing the parameters of feature extraction. See 
%   img2drkey for details.
%
% Output:
%   CC = a cell storing the indices to the features on each repttile 
%   (connected components).
%   f = a matrix storing the frame of one SIFT keypoint per column.
%   assgn_kNN = a matrix storing indices of the topN nearest visual words 
%   per column.
%   img = the loaded image (used for visualization).
%
% A. Torii, torii@ctrl.titech.ac.jp
% 14/05/2014

%--- actual processing begins
% img = rgb2gray(imread(imfn));
% if max(size(img)) > 1024,
%   img = imresize(img,1024/max(size(img)));
% end;
% [f, desc] = at_img2drkey(ps.drtype,img,vlft); % get descriptor for image I <SIFT or ROOTSIFT>
img = [];
% [f, desc] = readHessAffSIFT(imfn);
% [f, desc] = readFeaturePerdoch('./data/oxbuild_perdoch', imfn);
% [f, desc] = readFeaturePerdoch('./data/oxbuild_query_perdoch', imfn);
% [f, desc] = readFeaturePerdoch('./data/SUN/feature', imfn);
[f, desc] = readFeaturePerdoch('./data/HCM/feature-query', imfn);
desc = yael_fvecs_normalize(single(desc)); % normalize so that sum(vector desc) = 1

topN = ps.topN; % get K closest vocab/cluster point [50]
vvsize = size(CX,2); % visual vocabulary size --> aka: num of cluster
scalethr = ps.scalethr;
edgethr  = ps.edgdthr;

Np = size(f,2); fprintf(1,'%d keypoints\n',Np); % number of keypoint
if Np>0 
%   [assgn_kNN, ~] = yael_nn(single(CX), single(desc), topN); % replace by Approximate Nearest Neighbor(ANN/FLANN) to speed up.
%     tic;
%   [assgn_kNN, ~] = vl_kdtreequery(CXtree, CX, desc, 'NUMNEIGHBORS', 50, 'MaxComparisons', 700);
%   toc
    assgn_kNN = load(assgn_kNN_path);
    assgn_kNN = assgn_kNN.bins;
    if  Np < 10000
      
      Mall = sparse(vvsize,Np);
      for tt=1:topN
        assgn = assgn_kNN(tt,:);
        aid = 1:length(assgn(:));
        M = sparse(double(assgn(:)),aid,1,vvsize,Np);
        Mall = Mall + M;    
      end;
      % build histogram for vocabulary assignment aka translate index
      % assignment to histogram

      %--- integrate graphs and cut too long edges on the image
      A = (Mall'*Mall)>0;  
      B = triu(A); B = B - diag(diag(B));

      E = sparse(Np,Np); % graph edge
      S = sparse(Np,Np); % scale
      D = sparse(Np,Np); % mean scale
      points = single(f(1:2,:));
      info = single(f(3:end,:));
      for ii=1:Np
        [mi,mj] = find(B(ii,:));
        if ~isempty(mi)
          d = sqrt((points(1,mj) - points(1,ii)).^2 + (points(2,mj) - points(2,ii)).^2);
          E(ii,mj) = d;
          angle1 = atan(2.*info(2,mj) / (info(3,mj) - info(1,mj))) ./ 2;
          angle2 = atan(2.*info(2,ii) / (info(3,ii) - info(1,ii))) ./ 2;
          scale1 = (info(1,mj) + info(3,mj))/2 + ((info(1,mj) - info(3,mj))/2)* cos(2*angle1);
          scale2 = (info(1,ii) + info(3,ii))/2 + ((info(1,ii) - info(3,ii))/2)* cos(2*angle2);
          d = scale1./scale2;
          S(ii,mj) = d;
          d = scale1 + scale2./2; % mean of scales of pair of features
          D(ii,mj) = max(5,d);
        end;
      end;

      E = sparse(E); E = E + E';
      S = sparse(S); S = S + S';
      D = sparse(D); D = D + D';

      % scalethr = 0.5;
      [si,sj,sk] = find(S(:));
      A(si(sk<scalethr)) = 0;
      A(si(sk>(1/scalethr))) = 0;

      Z = E > edgethr*D;
      A(Z(:)) = 0;
    end
  if  Np >=10000
      A = sparse(Np,Np);
  end
  %--- find connected components in the graph, breath first search
  [bp, bq, br, bs] = dmperm(A);
  
  Nc = length(br)-1;
  CC = cell(1,Nc);
  
  ncVsz = zeros(1,Nc);
  for ii=1:Nc
    id = bp(br(ii):br(ii+1)-1);
    ncVsz(ii) = length(id);
    CC{ii} = id;
  end;
  [~,cidx] = sort(ncVsz,'descend');  
  CC = CC(cidx);
else
    CC = [];
    f = [];
    assgn_kNN = [];
    img = [];
end
