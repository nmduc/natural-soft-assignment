datasetFolder = './data/HCM/HCMlandmark';
processedDatasetFolder = './data/HCM/HCMlandmark';
groundtruthFile = './data/HCM/groundtruth';


createDir(processedDatasetFolder);
createDir(groundtruthFile);

[nSubFolder, datasetSubFolders] = readImageDir(datasetFolder);
groundtruthCell =  cell(nSubFolder,1);

for i=1:nSubFolder
    curFolder = datasetSubFolders(i);
    if curFolder.isdir == 1
        subFolderPath = [datasetFolder '/' curFolder.name];
        [nImageSubFolder, subFolderImagesFile] = readImageDir(subFolderPath);
        imagePrefix = lower(curFolder.name);
        imagePrefix = strrep(imagePrefix, ' ', '_');
        groundtruthCell{i,1} = cell(nImageSubFolder,1);
        for j = 1:nImageSubFolder
            img = imread([subFolderPath '/' subFolderImagesFile(j).name]);
            if size(img,2) > 1024
                img = imresize(img, 1024/size(img,2));
            end
            imgName = [imagePrefix '_' num2str(j, '%09d') '.jpg'];
            imwrite(img, [processedDatasetFolder '/' imgName]);
            groundtruthCell{i,1}{j,1} = imgName;
        end
    end
    fprintf('%d\n', i);
end
save([groundtruthFile '/' 'groundtruth.mat'], 'groundtruthCell');