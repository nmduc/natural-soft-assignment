function hmat = ComputeH( im1_pts, im2_pts )
%COMPUTEH Summary of this function goes here
%   Detailed explanation goes here
% h is vector 9x1
    h = zeros(9,1);
    h(9) = 1;
    A = [];
    for i = 1:4
        a1 = [-im1_pts(1,i), -im1_pts(2,i), -1, 0, 0, 0, im2_pts(1,i)*im1_pts(1,i), im2_pts(1,i)*im1_pts(2,i), im2_pts(1,i)];
        A = [A; a1];
        a2 = [0,0,0, -im1_pts(1,i), -im1_pts(2,i), -1, im2_pts(2,i)*im1_pts(1,i), im2_pts(2,i)*im1_pts(2,i), im2_pts(2,i)];
        A = [A; a2];
    end
    [U,S,V] = svd(single(A));
    h = V(:,end);
    hmat = [h(1), h(2), h(3); h(4), h(5), h(6); h(7), h(8), h(9)];
    hmat = hmat ./ h(9);
end

