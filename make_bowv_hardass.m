addpath ./practical-instance-search
addpath ./yael_v300/matlab

datasetImagePath = './data/oxbuild_images';
queryImagePath = './data/query-images';

datasetNNpath = './data/oxbuilding-50';
queryNNpath = './data/query-50';

datasetBOWpath = './data/bow_origin_rootsift/oxbuilding';
queryBOWpath = './data/bow_origin_rootsift/query';

   
createDir(queryBOWpath);
createDir(datasetBOWpath);
%[nImages, imagePaths] = readImageDir(queryNNpath);
[nImages, imagePaths] = readImageDir(datasetNNpath);
    
parfor i=1:nImages
    %kNN = load([ queryNNpath '/' imagePaths(i).name]);
    kNN = load([ datasetNNpath '/' imagePaths(i).name]);
    
    kNN = kNN.bins;
    kNN = kNN(1,:);
    h = sparse(double(kNN), 1, 1, 1e6, 1) ;
                       
    %parForSave([queryBOWpath '/' imagePaths(i).name], h);
    parForSave([datasetBOWpath '/' imagePaths(i).name], h);
    i
end;
   
   