function [ f, desc ] = readHessAffSIFT( filename )
%READHESSAFFSIFT Summary of this function goes here
%   Detailed explanation goes here
    features = load(filename);
    f = features.clip_kp;
    desc = features.clip_desc;
end

