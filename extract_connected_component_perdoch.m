%% load libraries
%--- paths to the libraries
addpath ./yael_v300/matlab
addpath ./practical-instance-search
if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end

%% define env var

%--- parameters 
ps.drtype = 'RSIFT';  % type of feature (default)
ps.vvsize = 1000000;   % size of visual vocabulary
ps.topN     = 50;     % max. kNN assignment ("K" in the paper)
ps.edgdthr  = 10;     % threshold of max. feature distances ("c")
ps.scalethr = sqrt(3.)/ 2;    % threshold of scale differences ("sigma")
ps.wmax = 3;          % max. number of adaptive assignments ("k_max")
ps.cutoff_thr = 1;    % threshold of truncating bovw weights ("T")
vlft = at_retrieve_vlparams(ps.drtype);

% assgn_kNN_read_path = './data/quantize/oxbuild-query-perdoch-50';
% data_path = './data/oxbuild_query_perdoch/desc';
% assgn_kNN_read_path = './data/SUN/quantize/SUN';
% data_path = './data/SUN/feature/desc';
assgn_kNN_read_path = './data/HCM/quantize-query/HCM';
data_path = './data/HCM/feature-query/desc';
model_path = './data/oxbuild_perdoch/Clustering_l2_1000000_13516675_128_50it.hdf5'; % hdf5 file format

% visual_word_encoding_feature = './data/visualwordhessaff';
% image_feature_visual_word_assignment_path = './data/imagefeaturevwhassaff';


% connectedcompresult = './data/SUN/connectedcomp_perdoch/SUN';
connectedcompresult = './data/HCM/connectedcomp_perdoch_query/HCM';
createDir(connectedcompresult);

% createDir(visual_word_encoding_feature);
% createDir(image_feature_visual_word_assignment_path);
%% load model from predefined files
BoWModel = loadHDF5BoWModel(model_path);

%% build feature graph and find repttile
% [nImages, fileNames, filePaths, nClass, classDir, imageDirVar] = countImagesInDir(data_path);
[nImages, imagePaths] = readImageDir(data_path);
[~, assgn_kNN_files] = readImageDir(assgn_kNN_read_path);
% for i=1:nClass
%     createDir([visual_word_encoding_feature '/' classDir(i)]);
% end
kdTreePath = './data/oxbuild_perdoch/kdtree.mat';
CXtree = [];
if exist(kdTreePath)==0
    CXtree = vl_kdtreebuild(BoWModel);
    save(kdTreePath, 'CXtree');
else
    CXtree = load(kdTreePath);
    CXtree = CXtree.CXtree;
end

% parForSave(['./data/result/codebook_kdtree.mat'], CXtree);
for i=1:nImages
    [CC, f, assgn_kNN, img] = at_img2repttile(BoWModel, CXtree,[ imagePaths(i).name], ps, vlft, [assgn_kNN_read_path '/' assgn_kNN_files(i).name]);
%     [v, K] = at_makebovw(CC, assgn_kNN, '', ps);
%     [f, assgn_kNN,img ] = soft_assignment(BoWModel, CXtree, [ data_path '/' imagePaths(i).name], ps, vlft, 3);
%     v = make_normal_bowv(assgn_kNN, '', ps);
%     image_visual_word = [visual_word_encoding_feature '/' imagePaths(i).name '.mat'];
%     parForSave([assgn_kNN_result '/' imagePaths(i).name '.mat'], assgn_kNN);
    parForSave([connectedcompresult '/' imagePaths(i).name '_connected.mat'], CC);
%     parForSave(image_visual_word, v);
%     parForSave([image_feature_visual_word_assignment_path '/' imagePaths(i).name '_features.mat'], assgn_kNN);
    i
end
