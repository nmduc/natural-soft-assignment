function extract_feature_diff(frame_path, save_path)
    createDir(save_path);
    kpPath = [save_path '/' 'kp'];
    descPath = [save_path '/' 'desc'];
    createDir(kpPath);
    createDir(descPath);
    feature_config = '-hesaff -rootsift -noangle';
    feat_len = 128;
    feature_dir = strrep(feature_config, '-', '');
    feature_dir = strrep(feature_dir, ' ', '_');
    exe = 'C:/Users/Administrator/Desktop/Nguyen Minh Duc/Workplace/MATLAB/data/haff_cvpr/haff_cvpr09.exe';
    run('./practical-instance-search/vlfeat/toolbox/vl_setup');
    sift_filename = './temp.sift';
    suffix = '.hesaff.sift';
    [~, D] = readImageDir(fullfile(frame_path));
    fil={D.name};  
    sumFeature = 0 ;
    for k=1:numel(fil)
        tic;
        file=fil{k};
        fileFullPath = [frame_path '/' file];
        fileInfo = dir(fileFullPath);
%         fileSize = fileInfo.bytes;
        
        fileName = fileInfo.name;
        startIndex = findstr(fileName, '.');
%         if ~isempty(startIndex)
%             continue;
%         end
%         cmd = sprintf('%s %s -i %s -o1 %s', exe, strrep(feature_config,'root',''), file, sift_filename);
        cmd = sprintf('"%s" "%s"', exe, fileFullPath);
        system(cmd);
        [clip_kp,clip_desc] = vl_ubcread(sift_filename, 'format', 'oxford');
        if ~isempty(strfind(feature_config,'rootsift'))
                    sift = double(clip_desc);
                    clip_desc = single(sqrt(sift./repmat(sum(sift), feat_len, 1)));
        end
%         clip_frame = rgb2gray(imread(file));
%         save(strcat([save_path '/' fileName(end-4:end)], '.mat'), 'clip_kp', 'clip_desc', '-v7.3');
         sumFeature = sumFeature + size(clip_kp,2);
        save(strcat([kpPath '/' fileName(1:end-4)], '.mat'), 'clip_kp', '-v7.3');
        save(strcat([descPath '/' fileName(1:end-4)], '.mat'), 'clip_desc', '-v7.3');
        toc
        fprintf('%d\n', k);
    end
    fprintf('%d/n',sumFeature);
end


function saveDescKey(clip_desc, descPath, clip_kp, keypointPath)
    save(keypointPath, 'clip_kp', '-v7.3');
    save(descPath, 'clip_desc', '-v7.3');
end