function [matchIdx, query_exp_vect, nVerifiedImage] = query_geo_verifi_inlier_project(matchIdx,nTop,data_Path,data_Files,datasetVects,queryFeature,queryKP,query_vector)

% data_Path = {datasetKeyPointPath; datasetFeaturePath};
% data_Files = { datasetKeyPointFiles ; datasetFeatureFiles};

nVerifiedImage = 0;
datasetKeyPointPath = data_Path{1,1};
datasetFeaturePath = data_Path{1,2};
datasetKeyPointFiles = data_Files{1,1};
datasetFeatureFiles = data_Files{1,2};
% weights = 1./(2.^((1:3)-1));
weights = ones(3,1);
geometricRerankSubset = matchIdx(1:nTop);
inliersCount = zeros(length(matchIdx),1);
inliersSave = cell(length(matchIdx),1);
loadedFeaturesSave = cell(length(matchIdx),1);
loadedKPSave = cell(length(matchIdx),1);
for i=1:nTop
    imgIdx = geometricRerankSubset(i);
    datasetKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(imgIdx).name]);
    loadedKPSave{imgIdx} = datasetKP;
    datasetFeature = loadFeatures([datasetFeaturePath '/' datasetFeatureFiles(imgIdx).name]);
    loadedFeaturesSave{imgIdx} = datasetFeature;
    datasetMatchesWords = matchWords_n_head(queryFeature(1:3,:), datasetFeature(1:3,:));
    [datasetInliers] = geometricVerification(queryKP,datasetKP, datasetMatchesWords,'numRefinementIterations', 3);
    inliersCount(i,1) = numel(datasetInliers); % follow current ranklist order
    inliersSave{imgIdx,1} = datasetMatchesWords(:,datasetInliers);
     if inliersCount(i,1) > 20
%         inlierPoints = queryKP(1:2, datasetMatchesWords(1, datasetInliers));
%         ROI = convhull(inlierPoints(1,:), inlierPoints(2,:));
%         ROI = inlierPoints(:, ROI);
%         queryVerifiedArea = polyarea(ROI(1,:), ROI(2,:));%         inliersCount(i,1) = queryVerifiedArea;
     else 
%         queryVerifiedArea = 0 ;
         inliersCount(i,1) = -1;
     end
%     if inliersCount(i,1) > 20 && queryVerifiedArea > 1000
%         inlierPoints = datasetKP(1:2, datasetMatchesWords(2, datasetInliers));
%         ROI = convhull(inlierPoints(1,:), inlierPoints(2,:));
%         ROI = inlierPoints(:, ROI);
%         acceptedPoints = inpolygon(datasetKP(1,:),datasetKP(2,:),ROI(1,:),ROI(2,:));
% %         acceptedPoints = datasetKP(acceptedPoints);
% %         verifiedBins = datasetFeature(1:3, datasetMatchesWords(2, datasetInliers));
%         verifiedBins = datasetFeature(1:3, acceptedPoints);
%         verifiedBins = verifiedBins(:);
%         verifiedBins = sparse(verifiedBins, ones(numel(verifiedBins),1), repmat(weights,1,numel(verifiedBins)/3), 1e6, 1);
%         verifiedBins = datasetVects(:,geometricRerankSubset(i)).*verifiedBins;
% %         verifiedBins = verifiedBins ./ norm(verifiedBins);
%         query_exp_vect = query_exp_vect + verifiedBins;
%         nVerifiedImage = nVerifiedImage + 1;
%     end
% %     i
end
negativeImages = (inliersCount==-1);
negativeImages = matchIdx(negativeImages);
negativeImagesVect = sum(datasetVects(:,negativeImages), 2);
[~, inlierOrder] = sort(inliersCount, 'descend');
% matchIdx(1:nTop) = matchIdx(inlierOrder);
matchIdx = matchIdx(inlierOrder);

query_exp_vect = sparse(1e6, 1);
nVerifiedImage = 20;
topN = matchIdx(2:nVerifiedImage-1);
queryVerifiedFeature = inliersSave{topN};
queryVerifiedFeature = unique(queryVerifiedFeature(1, :));
queryVerifiedFeature = queryFeature(1:3, queryVerifiedFeature);
queryVerifiedFeature = sparse(queryVerifiedFeature, ones(numel(queryVerifiedFeature),1), repmat(weights,1,numel(queryVerifiedFeature)/3), 1e6, 1);
queryVerifiedFeature = queryVerifiedFeature ./ norm(queryVerifiedFeature);
query_exp_vect = query_exp_vect + queryVerifiedFeature;

for i=2:length(topN)-1
    imgIdx = topN(i);
    datasetKP = loadedKPSave{imgIdx};
    datasetFeature = loadedFeaturesSave{imgIdx};
    inlierIdx = inliersSave{imgIdx};
    inlierCount = size(inlierIdx, 2);
    if inlierCount > 20
%         inlierKP = datasetKP(1:2, inlierIdx(2, :));
        inlierFeature = datasetFeature(1:3, inlierIdx(2, :));
        inlierFeature = inlierFeature(:);
        inlierFeature = sparse(inlierFeature, ones(numel(inlierFeature),1), repmat(weights,1,numel(inlierFeature)/3), 1e6, 1);
        inlierFeature = inlierFeature ./ norm(inlierFeature);
        query_exp_vect = query_exp_vect + inlierFeature;
    end
end

if nVerifiedImage~=0
     query_exp_vect = query_exp_vect ./ nVerifiedImage;
%     query_exp_vect = query_exp_vect ./ norm(query_exp_vect);
end