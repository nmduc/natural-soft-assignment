nVoc = 1e6;
data_set = './data/bow_hessaff_k_max_3_with_burst/oxbuilding';
result_path = './data/bow_hessaff_k_max_3_with_burst'; % has idf, df, posting_list
createDir(result_path);
[nFiles, files] = readImageDir(data_set);
inverse_index = cell(nVoc ,1);
% build tf-idf
for i=1:nFiles
    visual_hist_path = [data_set '/' files(i).name];
    x = load(visual_hist_path);
    temp_idx = find(x.v > 0);
    for j=1:length(temp_idx)
        inverse_index{temp_idx(j),1} = [inverse_index{temp_idx(j),1} i];
    end
    i
end
save([result_path '/' 'inverted_index.mat'], 'inverse_index'); % posting list