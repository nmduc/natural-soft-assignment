queryImageVWPath = './data/bow_hessaff_k_max_3_with_burst/query';
dataset_imageVW_path = './data/bow_hessaff_k_max_3_with_burst/oxbuilding';

datasetImagePath = './data/oxbuild_images';
resultPath = './data/result_hessaff_k_max_3_with_burst_tf_idf';
createDir(resultPath);
idf_path =  './data/bow_hessaff_k_max_3_with_burst/tf_idf.mat';
posting_list_path = './data/bow_hessaff_k_max_3_with_burst/inverted_index.mat';
datasetVects_path = './data/bow_hessaff_k_max_3_with_burst/datasetVects.mat';

idf = load(idf_path);
idf = idf.tf_idf;

nVoc = 1e6;
posting_list = load(posting_list_path);
posting_list = posting_list.inverse_index;
[nQueryImages, files] = readImageDir(queryImageVWPath);
[nDatasetImage, datasetFiles] = readImageDir(dataset_imageVW_path);
[~, datasetImageFiles] = readImageDir(datasetImagePath);


if matlabpool('size') == 0 % checking to see if my pool is already open
    matlabpool open 4
end
% load dataset vectors
datasetVects = [];
if exist(datasetVects_path, 'file') == 0
    datasetVects = sparse(nVoc, nDatasetImage);
    parfor i=1:nDatasetImage
        dataset_vector = load([dataset_imageVW_path '/' datasetFiles(i).name]);
        datasetVects(:,i) = dataset_vector.v;
        i
    end
    save(datasetVects_path, 'datasetVects');
else
    datasetVects = load(datasetVects_path);
    datasetVects = datasetVects.datasetVects;
end

tf_idf_diag = spdiags(tf_idf, 0, nVoc, nVoc);
%%
for j=1:nQueryImages
    imageVWPath = [queryImageVWPath '/' files(j).name];
    query_vector = load(imageVWPath);
    query_vector = query_vector.v;
    query_bins = find(query_vector>0);
    num_of_intersection = zeros(nDatasetImage, 1);


    matched_image_idx = [posting_list{query_bins}];
    bin_matched_image = 1:length(datasetImageFiles);
    matched_same_bin_count = histc(matched_image_idx, bin_matched_image);
    max_same_bin_count = max(matched_same_bin_count);
    sameBinTheshold = prctile(matched_same_bin_count, 0);
    matched_image_idx = find(matched_same_bin_count > sameBinTheshold);
%     matched_images_matrix = sparse(length(matched_image_idx),nVoc);
    
%     length(matched_image_idx)
    
%     for i=1:length(matched_image_idx)
      matched_images_matrix = tf_idf_diag*datasetVects(:, matched_image_idx);
%     end
    matched_scores = (query_vector.*tf_idf)'*(matched_images_matrix);
    [~, matched_order] = sort(matched_scores, 'descend');
    fileId = fopen([resultPath '/' files(j).name(1:end-4) '.txt'], 'w');
    for i=1:length(matched_scores)
        %matched_image_path = [datasetImagePath '/' datasetImageFiles(matched_image_idx(matched_order(i))).name];
        filename = datasetImageFiles(matched_image_idx(matched_order(i))).name;
        filename = filename(1:end-4);
        fprintf(fileId, '%s\n', filename);
    end
    fclose(fileId);
    fprintf('%d\n', j);
end
