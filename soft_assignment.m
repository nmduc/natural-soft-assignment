function [ f,assgn_kNN,img ] = soft_assignment( CX, CXtree, imfn, ps, vlft, num_assn )
%SOFT_ASSIGNMENT Summary of this function goes here
%   Detailed explanation goes here
    img = rgb2gray(imread(imfn));
    if max(size(img)) > 1024,
      img = imresize(img,1024/max(size(img)));
    end;
    [f, desc] = at_img2drkey(ps.drtype,img,vlft); % get descriptor for image I <SIFT or ROOTSIFT>
    desc = yael_fvecs_normalize(single(desc)); % normalize so that sum(vector desc) = 1

    topN = ps.topN; % get K closest vocab/cluster point [50]
    vvsize = size(CX,2); % visual vocabulary size --> aka: num of cluster

    Np = size(f,2); fprintf(1,'%d keypoints\n',Np); % number of keypoint
    if Np>0  
%   [assgn_kNN, ~] = yael_nn(single(CX), single(desc), topN); % replace by Approximate Nearest Neighbor(ANN/FLANN) to speed up.
        tic;
        [assgn_kNN, ~] = vl_kdtreequery(CXtree, CX, desc, 'NUMNEIGHBORS', num_assn, 'MaxComparisons', 1000);
        toc
    end
end

