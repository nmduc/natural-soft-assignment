function [matchIdx, query_exp_vect, nVerifiedImage] = query_geo_verifi_inlier_rerank(matchIdx,nTop,data_Path,data_Files,datasetVects,queryFeature,queryKP,query_vector)

% data_Path = {datasetKeyPointPath; datasetFeaturePath};
% data_Files = { datasetKeyPointFiles ; datasetFeatureFiles};
query_exp_vect = sparse(1e6, 1);
nVerifiedImage = 0;
datasetKeyPointPath = data_Path{1,1};
datasetFeaturePath = data_Path{1,2};
datasetKeyPointFiles = data_Files{1,1};
datasetFeatureFiles = data_Files{1,2};

geometricRerankSubset = matchIdx(1:nTop);
inliersCount = zeros(nTop,1);
for i=1:nTop
    datasetKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(geometricRerankSubset(i)).name]);
    datasetFeature = loadFeatures([datasetFeaturePath '/' datasetFeatureFiles(geometricRerankSubset(i)).name]);
    datasetMatchesWords = matchWords_n_head(queryFeature(1:3,:), datasetFeature(1:3,:));
    datasetInliers = geometricVerification(queryKP,datasetKP,datasetMatchesWords,'numRefinementIterations', 3);
    inliersCount(i,1) = numel(datasetInliers);
  
    if inliersCount(i,1) > 20
        verifiedBins = datasetFeature(1:3, datasetMatchesWords(2, datasetInliers));
        verifiedBins = verifiedBins(:);
        verifiedBins = sparse(verifiedBins, ones(numel(verifiedBins),1), ones(numel(verifiedBins),1), 1e6, 1) > 0;
        verifiedBins = datasetVects(:,geometricRerankSubset(i)).*verifiedBins;
        query_exp_vect = query_exp_vect + verifiedBins;
        nVerifiedImage = nVerifiedImage + 1;
    end
% %     i
end


% [~, inlierOrder] = sort(inliersCount, 'descend');
% matchIdx(1:nTop) = matchIdx(inlierOrder);


if nVerifiedImage~=0
    query_exp_vect = query_exp_vect ./ nVerifiedImage;
    query_exp_vect = query_exp_vect ./ norm(query_exp_vect);
end
[~, matchIdx] = query_cosin_similarity(datasetVects,query_exp_vect);
