function matches = matchWords_n_head(a, b)
% MATCHWORDS Matches sets of visual words
%   MATCHES = MATCHWORDS(A, B) finds occurences in B of each element
%   of A. Each matched pair is stored as a row of the 2xN matrix A,
%   such that A(MATCHES(1,i)) == B(MATCHES(2,i)).
%
%   By default, if an element of A matches to more than one element of
%   B, only one of the possible matches is generated.

% Author: Andrea Vedaldli
a = a(1:3, :);
b = b(1:3, :);
maxNumMatches = 1 ;

for i=1:maxNumMatches
  [ok1, m1] = ismember(a(1,:), b(1,:)) ;
  [ok2, m2] = ismember(a(2,:), b(2,:)) ;
  [ok3, m3] = ismember(a(1,:), b(2,:)) ;
  [ok4, m4] = ismember(a(2,:), b(1,:)) ;
  matches{i} = unique([find(ok1) find(ok2) find(ok3) find(ok4); m1(ok1) m2(ok2) m3(ok3) m4(ok4)]', 'rows')';
  total = size(matches{i},2);
  shuffle = randperm(total);
%   shuffle = shuffle(1:min(1000,total));
  matches{i} = matches{i}(:, shuffle);
%   b(m(ok1)) = b() = NaN ;
end
matches = cat(2, matches{:}) ;
