function [f, desc] = at_img2drkey(drtype,img,opt)
% Detect and describe features
% 
% Input:
%   drtype = a type of features to extract, "SIFT" or "RSIFT".
%   img = an input image [nrows, ncols, c]. 
%   opt = options for feature detection and description. See vl_covdet.m 
%   for details. 
%
% Output:
%   f = a matirx storing the frame of one SIFT keypoint per column in the
%   [X;Y;S;TH] format, where X, Y is the frame center, S its scale,
%   and TH its orientation. See vl_covdet.m for details.
%   desc = a matirx storing the SIFT descriptor per column. 
%
% A. Torii, torii@ctrl.titech.ac.jp
% 15/06/2013

if nargin < 3
  LV = 3; FO = 0; PK = 0; ED = 10; MG = 3; OR = true; AA = false;
else
  LV = opt.LV; FO = opt.FO; PK = opt.PK; ED = opt.ED; MG = opt.MG; OR = opt.OR; AA = opt.AA;
end;
if (size(img,3)>1)
  img=rgb2gray(img);
end;

tic
switch drtype
  case {'SIFT','RSIFT'}
    [f,desc] = vl_covdet(single(img),'Method','DoG','AffineAdaptation',AA,'Orientation',OR,'NumLevels',LV,'FirstOctave',FO,'PeakThreshold',PK,'EdgeThreshold',ED,'Magnif',MG);
    while size(desc,2)>20000, 
      FO = FO + 1;
      [f,desc] = vl_covdet(single(img),'Method','DoG','AffineAdaptation',AA,'Orientation',OR,'NumLevels',LV,'FirstOctave',FO,'PeakThreshold',PK,'EdgeThreshold',ED,'Magnif',MG);
    end;
    if strcmp(drtype,'RSIFT')
      desc = double(desc);
      % L1 sum of descriptor
      d = 1./sum(abs(desc),1);
      desc = desc*sparse(1:size(desc,2),1:size(desc,2),d);
      desc = single(sqrt(full(desc)));
    end;
end;
fprintf(1,'%s detected %d keys in %f sec\n',drtype,size(f,2),toc);

