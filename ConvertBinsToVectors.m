function featureVect = ConvertBinsToVectors(bins)
 temp = [bins{:}];
 bin = unique(temp);
 
 if(size(bin,2) == 1)
    count = size(temp,2);
 else
    count = hist(temp,bin);
 end;
 featureVect = sparse(bin, 1,count, 1e6, 1);