connected_component_path = './data/connectedcomp/oxbuilding';
assign_kNN_path = './data/oxbuilding-50';
result_bow_path = './data/bow_hessaff_k_max_3_no_thres/oxbuilding';
% connected_component_path = './data/connectedcomp_oxbuild_perdoch/query';
% assign_kNN_path = './data/quantize/oxbuild-query-perdoch-50';
% result_bow_path = './data/bow_hessaff_k_max_3_perdoch/query';
% connected_component_path = './data/connectedcomp_oxbuild_perdoch/oxbuilding';
% assign_kNN_path = './data/quantize/oxbuild-perdoch-50';
% result_bow_path = './data/bow_hessaff_k_max_3_perdoch/oxbuilding';
% connected_component_path = './data/HCM/connectedcomp_perdoch/HCM';
% assign_kNN_path = './data/HCM/quantize/HCM';
% result_bow_path = './data/HCM/bow_perdoch';
% connected_component_path = './data/HCM/connectedcomp_perdoch_query/HCM';
% assign_kNN_path = './data/HCM/quantize-query/HCM';
% result_bow_path = './data/HCM/bow_perdoch_query';
% connected_component_path = './data/SUN/connectedcomp_perdoch/SUN';
% assign_kNN_path = './data/SUN/quantize/SUN';
% result_bow_path = './data/SUN/bow_perdoch';
createDir(result_bow_path);
tf_idf_path = '';

[nImages, assgnFiles] = readImageDir(assign_kNN_path);
[~, connectedcompFiles] = readImageDir(connected_component_path);

% options
ps.drtype = 'RSIFT';  % type of feature (default)
ps.vvsize = 1000000;   % size of visual vocabulary
ps.topN     = 50;     % max. kNN assignment ("K" in the paper)
ps.edgdthr  = 10;     % threshold of max. feature distances ("c")
ps.scalethr = 0.5;    % threshold of scale differences ("sigma")
ps.wmax = 3;          % max. number of adaptive assignments ("k_max")
% ps.cutoff_thr = 4;    % threshold of truncating bovw weights ("T") for gaussian distance e^-d^2/(2*sigma^2)
ps.cutoff_thr = 1;    % threshold of truncating bovw weights ("T") for

tf_idf = zeros(ps.vvsize, 1);

for i=1:nImages
    iCC = load([connected_component_path '/' connectedcompFiles(i).name]);
    ikNN = load([assign_kNN_path '/' assgnFiles(i).name]);
    if size(ikNN.bins,2) > 0
    %     iCC.v;
    %     ikNN.bins;
    %     [v, K] = at_makebovw(iCC.v, ikNN.bins, tf_idf_path, ps);
         [v, K] = at_makebovw_gaussian_weighting(iCC.v, ikNN.bins, ikNN.sqrdists, tf_idf_path, ps);
        tf_idf(find(v>0)) = tf_idf(find(v>0)) + 1;
        % save bag of visual word
        save([result_bow_path '/' assgnFiles(i).name(1:end-4)], 'v', 'K');
    else
        v = sparse(1e6,1);
        K = sparse(1e6,1);
        save([result_bow_path '/' assgnFiles(i).name(1:end-4)], 'v', 'K');
    end
    i
end

% calculate idf vector
% tf_idf = log(nImages./(1+abs(tf_idf)));
% if exist(tf_idf_path,'file')==0
%     save(tf_idf_path, 'tf_idf');
% end
