function [ v ] = make_normal_bowv( assgn_kNN, idf_fn, opt )
%MAKE_NORMALBOVW Summary of this function goes here
%   Detailed explanation goes here
    Np = size(assgn_kNN, 2);
    nHighest = assgn_kNN(1:opt.wmax,:);
    wbase = 1./(2.^(0:(opt.wmax-1))); % wmax = 3: maximum assignment is 3 so w_id components can only take 1, 1/2, 1/4
    weights = cell(1,Np);
    words = cell(1,Np);
    for jj=1:Np
      idx = 1:opt.wmax;
      weights{jj} = wbase(idx);
      words{jj}   = assgn_kNN(idx,jj)';
    end; % extract the k_f nearest neighbor from K=50 nearest neighbor
    words = [words{:}];
    % sum over all features of an image
    v = sparse(double(words(:)), ones(1,length(words)), [weights{:}],opt.vvsize,1);
    [v,~] = at_svnorm(v);
end

