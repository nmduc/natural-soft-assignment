
addpath ./practical-instance-search
addpath ./yael_v300/matlab

kdTreePath = './data/kdtree.mat';
model_path = './data/oxbuilding_codebook_l2_1000000_24464227_128_50it.hdf5';

datasetImagePath = './data/oxbuild_images';
queryImagePath = './data/query-images';

datasetNNpath = './data/oxbuilding-50';
queryNNpath = './data/query-50';

datasetBOWpath = './data/bow_hessianaff_origin_rootsift';
queryBOWpath = './data/bow_query_hessianaff_origin_rootsift';

BoWModel = loadHDF5BoWModel(model_path);
CXtree = [];
CXtree = load(kdTreePath);
   CXtree = CXtree.CXtree;
   
createDir(queryBOWpath);
createDir(datasetBOWpath);
[nImages, imagePaths] = readImageDir(datasetImagePath);
%[nImages, imagePaths] = readImageDir(queryImagePath);

opts.box = [] ;
opts.maxNumComparisons = 1024 ;
opts.feature = {'method' 'hessian' 'affineAdaptation'    logical(1)   'orientation'  logical(0)};
    
parfor i=1:nImages
   
    im = imread([ datasetImagePath '/' imagePaths(i).name]);
    % extract the features
    [frames,descrs] = getFeatures(im,opts.feature{:}) ;

    % quantize the features
    words = vl_kdtreequery(CXtree, BoWModel, descrs, ...
                           'maxNumComparisons', opts.maxNumComparisons) ;
    
    h = sparse(double(words), 1, 1, 10e6, 1) ;
                       
    parForSave([datasetBOWpath '/' imagePaths(i).name '.mat'], h);
    i
end;
   
   