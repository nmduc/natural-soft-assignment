imagePath = './data/HCM/HCMlandmark';

[nImages, imagesFiles] = readImageDir(imagePath);

for i = 1: nImages
    imgName = [imagePath '/' imagesFiles(i).name];
    img = imread(imgName);
    if size(img,2) > 1024
        img = imresize(img, 1024/size(img,2));
        imwrite(img, imgName);
    end
    i
end