function [v,d] = at_svnorm(a)

a = double(a);
d = 1./sqrt(sum(a.^2));
v = a*sparse(1:size(a,2),1:size(a,2),d);
if ~issparse(a)
  v = full(v);
end
