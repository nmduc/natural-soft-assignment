function matchIdx1 = query_discrimative(datasetVects,matchIdx,nAVG,exp_query_vect)

nImage = size(datasetVects,2);
nExpVect = 200;
%% SVM

training_instance_matrix = [datasetVects(:,matchIdx(1,1:nExpVect)) datasetVects(:,matchIdx(1,(nImage-nExpVect+1):end))]';
not_inliner_bins = find(exp_query_vect == 0);
training_instance_matrix(:,not_inliner_bins) = sparse(nExpVect*2,length(not_inliner_bins));

training_label_vector = [ones(1,nExpVect) zeros(1,nExpVect)]';
libsvm_option = '-s 0 -c 1 -t 3';

model = svmtrain(training_label_vector, training_instance_matrix ,libsvm_option);
[predict_label, accuracy, dec_values] = svmpredict(training_label_vector, training_instance_matrix,model);

[~ ,matchIdx_p] = sort(dec_values, 'descend');

temp = matchIdx(1,1:2*nExpVect);
matchIdx1 = matchIdx;
matchIdx1(1,1:nExpVect) = temp(1,matchIdx_p(1:nExpVect)');

query_vector = sum(datasetVects(:,matchIdx1(1,1:nAVG)),2);
%% New query
[score_c matchIdx_c] = query_cosin_similarity(datasetVects,query_vector);
matchIdx_f = matchIdx_c;
score_result = score_c;
score_result = score_result;

%% New rank lisk
[~, matchIdx1] = sort(score_result, 'descend'); %for vector metric