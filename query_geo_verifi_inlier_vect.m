function [matchIdx, query_exp_vect, nVerifiedImage] = query_geo_verifi_inlier_vect(matchIdx,nTop,data_Path,data_Files,datasetVects,queryFeature,queryKP)

% data_Path = {datasetKeyPointPath; datasetFeaturePath};
% data_Files = { datasetKeyPointFiles ; datasetFeatureFiles};
query_exp_vect = sparse(1e6, 1);
nVerifiedImage = 0;
datasetKeyPointPath = data_Path{1,1};
datasetFeaturePath = data_Path{1,2};
datasetKeyPointFiles = data_Files{1,1};
datasetFeatureFiles = data_Files{1,2};
% weights = 1./(2.^((1:3)-1));
weights = ones(3,1);
geometricRerankSubset = matchIdx(1:nTop);
inliersCount = zeros(length(matchIdx),1);
for i=1:nTop
    datasetKP = loadKeyPoint([datasetKeyPointPath '/' datasetKeyPointFiles(geometricRerankSubset(i)).name]);
    datasetFeature = loadFeatures([datasetFeaturePath '/' datasetFeatureFiles(geometricRerankSubset(i)).name]);
    datasetMatchesWords = matchWords_n_head(queryFeature(1:3,:), datasetFeature(1:3,:));
    
    datasetInliers = geometricVerification(queryKP,datasetKP,datasetMatchesWords,'numRefinementIterations', 3);
    inliersCount(i,1) = numel(datasetInliers);
  
    if inliersCount(i,1) > 20
        verifiedBins = datasetFeature(1:3, datasetMatchesWords(2, datasetInliers));
        verifiedBins = verifiedBins(:);
        verifiedBins = sparse(verifiedBins, ones(numel(verifiedBins),1), repmat(weights,1,numel(verifiedBins)/3), 1e6, 1) > 0;
        verifiedBins = datasetVects(:,geometricRerankSubset(i)).*verifiedBins;
        verifiedBins = verifiedBins ./ norm(verifiedBins);
        query_exp_vect = query_exp_vect + verifiedBins;
        nVerifiedImage = nVerifiedImage + 1;
    else
        inliersCount(i, 1) = 0;
    end
% %     i
end


[~, inlierOrder] = sort(inliersCount, 'descend');
% matchIdx(1:nTop) = matchIdx(inlierOrder);
matchIdx = matchIdx(inlierOrder);
if nVerifiedImage~=0
    query_exp_vect = query_exp_vect ./ nVerifiedImage;
%     query_exp_vect = query_exp_vect ./ norm(query_exp_vect);
end