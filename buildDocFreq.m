docFreqPath = './data/docFreqNSA';
docFreqName = 'docFreqNSA.mat';
createDir(docFreqPath);
visualWordFeaturePath = './data/visualwordfeatures'; % v
[nFeatureFiles, visualWordFeatureFiles] = readImageDir(visualWordFeaturePath);

nVoc = 1e6;
docFreq = zeros(nVoc,1);
for i=1:nFeatureFiles
    v = load([visualWordFeaturePath '/' visualWordFeatureFiles(i).name]);
    v = v.v;
    docFreq = docFreq + (v>0);
    i
end
save([docFreqPath '/' docFreqName]);

%% recal features dataset
docFreq(docFreq<=0) = 1;
docFreq(docFreq>nFeatureFiles) = nFeatureFiles;
idocfreq = log(nFeatureFiles./double(docFreq));
for i=1:nFeatureFiles
    featurePath = [visualWordFeaturePath '/' visualWordFeatureFiles(i).name];
    v = load(featurePath);
    v = v.v;
    v = v.*idocfreq;
    save(featurePath, 'v');
    i
end
%% recal features test
queryFeaturePath = './data/queryvw';
[nQueryFeatureFiles, visualWordQueryFeatureFiles] = readImageDir(queryFeaturePath);
for i=1:nQueryFeatureFiles
    featurePath = [queryFeaturePath '/' visualWordQueryFeatureFiles(i).name];
    v = load(featurePath);
    v = v.v;
    v = v.*idocfreq;
    save(featurePath, 'v');
    i
end