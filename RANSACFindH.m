function [ H, setOfAgreePoints ] = RANSACFindH( setOfPoint_1, desc_1,setOfPoint_2, desc_2 )
%RANSACFINDH Summary of this function goes here
%   Detailed explanation goes here
% setOfPoint_1 n*d points
% setOfPoint_2 n*d points

    Dist = 1 - (desc_1'* desc_2);
%      Dist =  pdist2(transpose(desc_1) ,transpose(desc_2));
    [Min, Pos] = min(Dist, [], 2);

    Dist(1:size(Min), Pos(1:size(Min))) = +Inf;

    % find the second closest vector
    [secondMin, ~] = min(Dist, [], 2);
    % A(n,I(n)) is the value
    
    PointPool = [];
    % i is from 2142 descrptor and from left img
    % Pos(i) is from 2213 descrptor and from right img
    len = size(Dist, 1);
%     Ratio = [];
    % remove the one which is not a high enough maximum
%     for i = 1 : len
%         Ratio = [Ratio, Min(i) / secondMin(i)  ];
            % array of ref to i is img1 keypoint
            % pos(i) is ref to img2 corres keypoint
%          PointPool = [PointPool [i ; Pos(i)]];
%     end
    PointPool = [1:len ; Pos(1:len)'];
    poolSize = size(PointPool, 2);
    inlinerCount = 0;
    bestHomography = [];
    for i = 1:20
        randPos = randperm(poolSize, 4);
        randPoint = PointPool(:, randPos);
        im1_pts = zeros(4,4);
        im2_pts = zeros(4,4);
        for j = 1:4
            im1_pts(:,j) = setOfPoint_1(1:4, randPoint(1,j));
            im2_pts(:,j) = setOfPoint_2(1:4, randPoint(2,j));
        end
        % call ComputeH to get homography matrix
        HomographyMat = ComputeH(im1_pts, im2_pts);
%         inliers = [];
        % compute the true homography with left image points
        tmp = 0;
        for j = 1:len
            % transpose HomographyMat
            warpedPnt = HomographyMat * [setOfPoint_1(1:2,j);1];
            warpedPnt = warpedPnt / warpedPnt(3);
            %Transformed = [Transformed, warpedPnt];
            distFromReal = norm(warpedPnt(1:2) - setOfPoint_2(1:2,Pos(j)));
            if distFromReal < 10
%                 inliers = [inliers, j];
                tmp = tmp + 1;
            end
        end
        if tmp > inlinerCount
            inlinerCount = tmp;
            bestHomography = HomographyMat;
            setOfAgreePoints = inlinerCount;
        end
    end
    H = bestHomography;
end

