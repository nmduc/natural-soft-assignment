function [ kp ] = loadKeyPoint( keyPointPath )
%LOADKEYPOINT Summary of this function goes here
%   Detailed explanation goes here
    temp = load(keyPointPath);
    try
        kp = temp.kp;
    catch 
        kp = temp.clip_kp;
    end
end

