imagePath = './data/paris_building';
queryImagePath = './data/paris_query';
groundTruthPath = './data/paris_groundtruth';
createDir(queryImagePath);

queryDesc = dir([groundTruthPath '/' '*_query.txt']);

for i=1:length(queryDesc)
    fileId = fopen([groundTruthPath '/' queryDesc(i).name], 'r');
    temp = textscan(fileId, '%s %f %f %f %f');
    queryImageName = temp{1}{1};
    queryImage = imread([imagePath '/' queryImageName '.jpg']);
    queryImage = queryImage(temp{3}:temp{5}, temp{2}:temp{4}, :);
    imwrite(queryImage, [queryImagePath '/' queryDesc(i).name(1:end-4) '.jpg']);
    fclose(fileId);
end