function [ bins, dists ] = loadKNNFeature( featurePath,KNN_SpatialRE )
%LOADFEATURES Summary of this function goes here
%   Detailed explanation goes here
    temp = load(featurePath);
    bins = temp.bins(1:KNN_SpatialRE,:);
%     nFeature = size(bins, 2);
%     nIndex = 1:nFeature;
%     nIndex = repmat(nIndex, size(bins, 1), 1);
%     featureVect = sparse(bins(:), nIndex(:), ones(size(bins, 2)*size(bins, 1),1), 1e6, nFeature);
%     bins = featureVect;
    dists = temp.sqrdists;
end