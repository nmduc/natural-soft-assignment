% starter

if ~exist('vl_version','file'),
  run('./practical-instance-search/vlfeat/toolbox/vl_setup') ;
end
% 
imagePath = './data/oxbuild_images';
datasetPrefix = './data/bow_perdoch_hard_assign';
datasetVectsPath = [datasetPrefix '/oxbuilding'];
augmentVectorResultPath = [datasetPrefix '/' 'augmentResultOxbuilding'];
createDir(augmentVectorResultPath);
% 
datasetImagePath = './data/oxbuild_images';
datasetFeaturePath = './data/quantize/oxbuild-perdoch-50';
datasetKPPath = './data/oxbuild_perdoch/kp';
inlierGraphPath = [datasetPrefix '/inlierGraph.mat'];
relationGraphPath = [datasetPrefix '/relationGraph.mat'];
homographyGraphPath = [datasetPrefix '/homographyGraph.mat'];


[nDatasetImage, datasetFiles] = readImageDir(datasetVectsPath);
[~, imageFiles] = readImageDir(imagePath);
[~, datasetImageFiles] = readImageDir(datasetImagePath);
% 
[~, datasetFeatureFiles] = readImageDir(datasetFeaturePath);
[~, datasetKPFiles] = readImageDir(datasetKPPath);


inlierGraph = load(inlierGraphPath);
inlierGraph = inlierGraph.inlierGraph;
relationGraph = load(relationGraphPath);
relationGraph = relationGraph.relationGraph;
homographyGraph = load(homographyGraphPath);
homographyGraph = homographyGraph.homographyGraph; % homography map point from query image -> datasetImage (i->j)

imageFeatures = cell(nDatasetImage, 1);
for i=1:nDatasetImage
    temp = load([datasetFeaturePath '/' datasetFeatureFiles(i).name]);
    imageFeatures{i,1} = temp.bins(1:3, :);
    i
end

imgSizes = zeros(2,nDatasetImage);
tic;
for i=1:nDatasetImage
    temp = size(imread([imagePath '/' imageFiles(i).name]));
    imgSizes(:,i) = temp(1:2);
end
toc

% weights = 1./2.^((1:3) - 1);
weights = [1,1,1];
range = [1:3];
% range = 1:3;
datasetVectsRaw = sparse(1e6, nDatasetImage);
parfor i=1:nDatasetImage
    dataset_vector = load([datasetVectsPath '/' datasetFiles(i).name]);
    datasetVectsRaw(:,i) = dataset_vector.v;
    i
end
augmentedResultMatrix = sparse(1e6,nDatasetImage);
for i=1:nDatasetImage
    tic;
    directConnectVert = find(relationGraph(i, :) > 0);
    geometricVerifiedFeatures = inlierGraph(i, directConnectVert);
    augmentResultVect = sparse(1e6, 1);
    queryImgBound = imgSizes(:, i);
%     rectBoundCoordX = [0 queryImgBound(1) queryImgBound(1) 0 0];
%     rectBoundCoordY = [0 0 queryImgBound(2) queryImgBound(2) 0];
    for j=1:numel(geometricVerifiedFeatures) % foreach image directly connect to the i_th image
        imgIdx = directConnectVert(j);
        if imgIdx == i
            continue;
        end
        augmentFeatureIdx = geometricVerifiedFeatures{j};
        augmentFeatureIdx = augmentFeatureIdx(2,:);

%         homographyMat = homographyGraph{i,imgIdx};
%         augmentImageKP = loadKeyPoint([ datasetKPPath '/' datasetKPFiles(imgIdx).name]);
%         augmentImageKP = [augmentImageKP(1:2,:); ones(1,size(augmentImageKP,2))];
%         augmentImageKP = inv(homographyMat)*augmentImageKP;
        
%         augmentFeatureIdx = inpolygon(augmentImageKP(1,:),augmentImageKP(2,:),rectBoundCoordX,rectBoundCoordY);

%         augmentFeatureIdx = find(augmentFeatureIdx>0);
%         randomChoice = randperm(length(augmentFeatureIdx));
%         randomChoice = randomChoice(1:min(length(augmentFeatureIdx), 1000));
%         augmentFeatureIdx = augmentFeatureIdx(randomChoice);
%         augmentFeatureIdx = geometricVerifiedFeatures{j};
%         augmentFeatureIdx = augmentFeatureIdx(2,:);
        connectedImageFeature = imageFeatures{imgIdx,1};
%         try
            augmentFeatures = connectedImageFeature(range, augmentFeatureIdx);
%         catch ME
%             continue;
%         end
        weightsTemp = repmat(weights(range), 1, length(find(augmentFeatureIdx)));
        tempVect = sparse(augmentFeatures(:)', ones(length(weightsTemp),1), weightsTemp, 1e6, 1);
        augmentResultVect = augmentResultVect + tempVect;
    end
%     if norm(augmentResultVect) > 0
%         augmentResultVect = augmentResultVect ./ norm(augmentResultVect);
%     end
    augmentResultVect = sparse(augmentResultVect);
    fprintf('done calculating %d\n', i);
    augmentedResultMatrix(:, i) = augmentResultVect;
    save([augmentVectorResultPath '/' datasetFiles(i).name], 'augmentResultVect');
%     fprintf('done saving');
    toc
end

