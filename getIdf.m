pathPrefix = './data/HCM';
bow_path = [pathPrefix '/' 'bow_perdoch'];
tf_idf_path = [pathPrefix '/' 'tf_idf.mat'];

[nImages, bowFiles] = readImageDir(bow_path);

% options
ps.drtype = 'RSIFT';  % type of feature (default)
ps.vvsize = 1000000;  % size of visual vocabulary
ps.topN     = 50;     % max. kNN assignment ("K" in the paper)
ps.edgdthr  = 10;     % threshold of max. feature distances ("c")
ps.scalethr = 0.5;    % threshold of scale differences ("sigma")
ps.wmax = 5;          % max. number of adaptive assignments ("k_max")
ps.cutoff_thr = 1;    % threshold of truncating bovw weights ("T")

tf_idf = zeros(ps.vvsize, 1);

for i=1:nImages
    v = load([bow_path '/' bowFiles(i).name]);
    v = v.v;
    tf_idf(find(v>0)) = tf_idf(find(v>0)) + 1;
    % save bag of visual word
    i
end

% calculate idf vector
tf_idf = log(nImages./(1+abs(tf_idf)));
if exist(tf_idf_path,'file')==0
    save(tf_idf_path, 'tf_idf');
end
