function matchIdx = query_avg_exp(datasetVects,score_p ,matchIdx_p,nExpVect)

%score va matchIdx - cosin similarity va rank list

%query expansion (AVG)

%% AVG vector
exp_vectors = datasetVects(:,matchIdx_p(1,1:nExpVect));
query_vector = sum(exp_vectors,2)/size(exp_vectors,2);

%% New query
[score_c matchIdx_c] = query_geo(datasetVects,query_vector);
matchIdx_f = matchIdx_c;
score_result = score_c;
score_result = score_result;

%% New rank lisk
[~, matchIdx] = sort(score_result, 'descend'); %for vector metric

