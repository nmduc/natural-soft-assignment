function [matchIdx, score_result] = query_avg_exp_with_verified_vector(datasetVects,score_p ,matchIdx_p,nExpVect, verified_vector, query_vector,nVerifiedImage)

%score va matchIdx - cosin similarity va rank list

%query expansion (AVG)
% query_vector = query_vector./norm(query_vector);

%% AVG vector
% exp_vectors = datasetVects(:,matchIdx_p(1,1:nExpVect));
% query_vector = sum(exp_vectors,2)/size(exp_vectors,2);
%  query_vector = (verified_vector + query_vector)./(1 + 1);
query_vector = verified_vector;

%% New query
[score_c matchIdx_c] = query_cosin_similarity(datasetVects,query_vector);
matchIdx_f = matchIdx_c;
score_result = score_c;
score_result = score_result;

%% New rank lisk
[score_result, matchIdx] = sort(score_result, 'descend'); %for vector metric

