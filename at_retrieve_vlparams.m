function vlft = at_retrieve_vlparams(drtype,varargin)

switch drtype
  case {'SIFT','RSIFT'}
    vlft.LV=3; vlft.FO=0; vlft.PK=1; vlft.ED=10; vlft.MG=3; vlft.OR=true; vlft.AA=false;
  case 'HESAFF'
    vlft.LV=3; vlft.FO=0; vlft.PK=28.5; vlft.ED=10; vlft.MG=3; vlft.OR=true; vlft.AA=true;
end

vlft = vl_argparse(vlft,varargin);
