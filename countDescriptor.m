% kpFolder = './data/oxbuild_perdoch/kp';
kpFolder = 'C:\Users\Administrator\Desktop\Nguyen Minh Duc\Workplace\MATLAB\data\SUN\feature\kp';


[nImages, imageKPFiles] = readImageDir(kpFolder);
nKP = 0;
morethan10KCount = 0;
maxNum = -1;
minNum = 1e10;
for i = 1:nImages
    kpFile = imageKPFiles(i);
    kp = loadKeyPoint([kpFolder '/' kpFile.name]);
    nKP = nKP + size(kp,2);
    if size(kp,2)>10000
        morethan10KCount = morethan10KCount+1;
    end
    if size(kp,2)>maxNum
        maxNum = size(kp,2);
    end
    if size(kp,2)<minNum
        minNum = size(kp,2);
    end
end
fprintf('%d\n',nKP);