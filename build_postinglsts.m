dataset_imageVW_path = './data/bow_hessaff_k_max_3_with_burst/oxbuilding';
datasetImagePath = './data/oxbuild_images';
datasetFeaturePath = './data/oxbuilding-50';
dataset_postinglists_path = './data/bow_hessaff_k_max_3_with_burst/';

if matlabpool('size') == 0
    matlabpool open 2
end
datasetVects = sparse(1e6, nDatasetImage);

parfor i=1:nDatasetImage
    dataset_vector = load([dataset_imageVW_path '/' datasetFiles(i).name]);
    datasetVects(:,i) = dataset_vector.v;
end;

Threshold = 0;
postinglists = datasetVects>Threshold;

save([dataset_postinglists_path '/postinglist.mat'],'postinglists'); 