function [dataset_vector dataset_idx] = query_boolean_retrieval(datasetVects,query_vector,postinglists)

n = size(datasetVects,2);
weight_threshold = 0;
bin_threshold = 50;

query_idx = find(query_vector>weight_threshold);

[row,col] = find(postinglists(query_idx,:)==1);

hist_docs = histc(col,1:n);
dataset_idx = find(hist_docs>bin_threshold);
dataset_vector = datasetVects(:,dataset_idx);