function extract_features(frame_path)
    feature_config = '-hesaff -rootsift -noangle';
    feat_len = 128;
    feature_dir = strrep(feature_config, '-', '');
    feature_dir = strrep(feature_dir, ' ', '_');
    exe = './tools/compute_descriptors.exe'; 
    run('./practical-instance-search/vlfeat/toolbox/vl_setup.m');
    sift_filename = '/home/bao/temp.mat';
    D = rdir(fullfile(frame_path, '**', '*'));
    fil={D.name};  
    for k=1:numel(fil)
        file=fil{k};
        fileInfo = dir(file);
        fileSize = fileInfo.bytes;
        if fileSize < 2048
            continue;
        end
        fileName = fileInfo.name;
        startIndex = findstr(fileName, '.');
        if ~isempty(startIndex)
            continue;
        end
        cmd = sprintf('%s %s -i %s -o1 %s', exe, strrep(feature_config,'root',''), file, sift_filename);
        unix(cmd);
        [clip_kp,clip_desc] = vl_ubcread(sift_filename, 'format', 'oxford');
        if ~isempty(strfind(feature_config,'rootsift'))
                    sift = double(clip_desc);
                    clip_desc = single(sqrt(sift./repmat(sum(sift), feat_len, 1)));
        end
        clip_frame = rgb2gray(imread(file));
        save(strcat(file, '.mat'), 'clip_kp', 'clip_desc', 'clip_frame', '-v7.3');
    end